# Performance of Stream-Processing-Systems, comparison of local and global coordination
## Bachelor Thesis by Daniel Sterchi

This is the Code-Repository for the Thesis, which contains the two Storm-Topologies used for the performance evaluation.
The Paper itself (written in German) can be found on the following link, and is briefly summarized by the Poster below.

### 📄 ➡ [Thesis as PDF](BachelorThesis_Sterchi_2022.pdf)

![Poster](BachelorThesis_Sterchi_2022_Poster.svg)
