package ch.novarx.ffhs;

import ch.novarx.ffhs.Components.StormConfig;
import ch.novarx.ffhs.spout.TextSpout;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ComponentsTest {
    @Test
    void should_add_correct_workload_to_spout() {
        var conf = new StormConfig();
        conf.workload = 1000;
        conf.replicaTextSpout = 5;

        var sut = new Components(conf);
        assertThat(((TextSpout) sut.textSpout).textPerSecond).isEqualTo(1000);
    }

    @Test
    void should_add_correct_workload_to_spout_one_replica() {
        var conf = new StormConfig();
        conf.workload = 1000;
        conf.replicaTextSpout = 1;

        var sut = new Components(conf);
        assertThat(((TextSpout) sut.textSpout).textPerSecond).isEqualTo(1000);
    }

    @Test
    void should_deserialize() {
        var json = "{\n" +
                "  \"workload\": 1000,\n" +
                "  \"failEveryNth\": 10000,\n" +
                "  \"replicas\": 2,\n" +
                "  \"replicaTextSpout\": 2,\n" +
                "  \"replicaSplitBolt\": 2,\n" +
                "  \"replicaCountBolt\": 2,\n" +
                "  \"replicaCommitBolt\": 2,\n" +
                "  \"topoConfig\": {\n" +
                "    \"topology.workers\": 20,\n" +
                "    \"topology.max.task.parallelism\": 5,\n" +
                "    \"topology.max.spout.pending\": 100,\n" +
                "    \"topology.spout.max.batch.size\": 100,\n" +
                "    \"topology.trident.batch.emit.interval.millis\": 1,\n" +
                "    \"topology.message.timeout.secs\": 10\n" +
                "  }\n" +
                "}\n";

        StormConfig config = new Gson().fromJson(json, StormConfig.class);

        assertThat(config.topoConfig.get("topology.spout.max.batch.size")).isEqualTo(100);
    }
}
