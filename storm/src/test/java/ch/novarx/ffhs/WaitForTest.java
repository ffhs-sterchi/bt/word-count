package ch.novarx.ffhs;

import ch.novarx.ffhs.app.WaitFor;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Offset.offset;

class WaitForTest {
    WaitFor sut = new WaitFor();

    @Test
    void should_create() {
        assertThat(sut).isNotNull();
    }

    @Test
    void waitsForSeconds() {
        long start = new Date().getTime();
        sut.forSeconds(1);
        long end = new Date().getTime();
        assertThat(end - start).isCloseTo(1000L, offset(100L));
    }
}
