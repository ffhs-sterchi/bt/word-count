package ch.novarx.ffhs.app;

import ch.novarx.ffhs.app.Message.Id;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.assertj.core.api.Assertions.assertThat;

class MessageTest {


    @Test
    void should_generate_id() {
        var text = "123";
        var sut = new Id(text);
        var hash = sha256Hex(text);
        assertThat(sut.toString()).matches("\\d+\\|\\|" + hash);
    }

    @Test
    void should_generate_id_from_string() {
        var text = "123";

        var sut = new Id(text);
        var copy = Id.of(sut.toString());

        assertThat(copy).hasToString(sut.toString());
    }

    @Test
    void should_get_latency_in_to_string() {
        var message = new Message();
        message.emittedAt = new Date(2022, 1, 1, 10, 0, 0);
        message.ackAt = new Date(2022, 1, 1, 10, 0, 2);

        assertThat(message.getLatencyMs()).isEqualTo(2000);
        assertThat(message.toString()).contains("\"latencyMs\": \"2000\"");
    }

}
