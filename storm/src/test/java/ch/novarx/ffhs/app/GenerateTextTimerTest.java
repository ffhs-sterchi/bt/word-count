package ch.novarx.ffhs.app;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Timer;

import static ch.novarx.ffhs.app.GenerateTextTimer.INTERVAL_MS;
import static org.assertj.core.api.Assertions.assertThat;

class GenerateTextTimerTest {
    GenerateTextTimer sut;

    @ParameterizedTest
    @CsvSource({
            "10",
            "20",
            "50",
            "100",
            "400",
            "1600",
            "3200",
    })
    void impactsTheEmitedTuples(int textPerSecond) {
        var messages = new ArrayList<Message>();
        sut = new GenerateTextTimer(textPerSecond, messages::add);

        for (int i = 0; i < (1000 / INTERVAL_MS); i++) {
            sut.run();
        }

        assertThat(messages).hasSize(textPerSecond);
    }

    @Test
    void should_emit_texts_in_given_interval() {
        var queue = new ArrayDeque<Message>();
        var timer = new Timer();
        var timerTask = new GenerateTextTimer(200, queue::add);

        timer.scheduleAtFixedRate(timerTask, INTERVAL_MS, INTERVAL_MS);
        new WaitFor().forSeconds(5);
        timer.purge();

        assertThat(queue).hasSizeBetween(980, 1020);
        assertThat(queue).allSatisfy(e -> assertThat(e.text).isNotEmpty());
    }
}
