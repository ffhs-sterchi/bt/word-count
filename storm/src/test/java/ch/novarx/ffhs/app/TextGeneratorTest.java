package ch.novarx.ffhs.app;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static ch.novarx.ffhs.app.Dictionary.ENGLISH_WORDS;
import static ch.novarx.ffhs.app.WordSplitter.WORD_SPLIT_PATTERN;
import static org.assertj.core.api.Assertions.assertThat;

class TextGeneratorTest {
    private final WordSplitter splitter = new WordSplitter();
    TextGenerator sut = new TextGenerator();

    @Test
    void should_create() {
        assertThat(sut).isNotNull();
    }

    @Test
    void should_have_list_of_valid_words() {
        assertThat(ENGLISH_WORDS).noneSatisfy(word -> assertThat(word).matches(WORD_SPLIT_PATTERN));
    }

    @Test
    void should_generate_text() {
        var all = new ArrayList<String>();
        for (int i = 0; i < 2000; i++) {
            var text = sut.randomText();
            all.add(text);
            assertThat(text).isNotNull();
            var splitted = splitter.split(text);
            assertThat(splitted).allSatisfy(word -> assertThat(ENGLISH_WORDS).contains(word));
            assertThat(splitted.size()).isGreaterThanOrEqualTo(10).isLessThanOrEqualTo(50);
            assertThat(text).isNotEqualTo(sut.randomText());
            assertThat(text).isNotEqualTo(sut.randomText());
        }
        assertThat(all).hasSize(2000).allSatisfy(t -> assertThat(t).isNotEmpty());
    }

}
