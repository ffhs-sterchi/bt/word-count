package ch.novarx.ffhs.app;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class FailerTest {
    @Test
    void should_not_fail() {
        var sut = new Failer();
        assertThat(sut.shouldFailEmittingMessage(0)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(0)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(0)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(0)).isFalse();
    }

    @Test
    void should_fail_every_2nd_tuple() {
        var sut = new Failer();
        assertThat(sut.shouldFailEmittingMessage(2)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(2)).isTrue();
        assertThat(sut.shouldFailEmittingMessage(2)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(2)).isTrue();
        assertThat(sut.shouldFailEmittingMessage(2)).isFalse();
    }

    @Test
    void should_fail_every_5th_tuple() {
        var sut = new Failer();
        assertThat(sut.shouldFailEmittingMessage(5)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(5)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(5)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(5)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(5)).isTrue();
        assertThat(sut.shouldFailEmittingMessage(5)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(5)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(5)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(5)).isFalse();
        assertThat(sut.shouldFailEmittingMessage(5)).isTrue();
    }
}
