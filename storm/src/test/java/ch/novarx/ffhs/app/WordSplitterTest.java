package ch.novarx.ffhs.app;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class WordSplitterTest {
    WordSplitter sut = new WordSplitter();

    @Test
    void should_split_two_words() {
        assertThat(sut.split("lorem ipsum")).containsExactly("lorem", "ipsum");
    }

    @Test
    void should_split_sentence_to_words() {
        String text = "Lorem ipsum, dolor sit amet. More? Or not...";
        String[] words = {"lorem", "ipsum", "dolor", "sit", "amet", "more", "or", "not"};
        assertThat(sut.split(text)).containsExactly(words);
    }

    @Test
    void should_split_failWord() {
        String text = "Lorem ipsum this_word_should_fail";
        String[] words = {"lorem", "ipsum", "this_word_should_fail"};
        assertThat(sut.split(text)).containsExactly(words);
    }
}
