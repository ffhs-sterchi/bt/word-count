package ch.novarx.ffhs.app;

import ch.novarx.ffhs.utils.MetricCollector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*;

import static ch.novarx.ffhs.utils.MetricCollector.percentile;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Percentage.withPercentage;

class MetricCollectorTest {
    public static final Random RANDOM = new Random();
    MetricCollector sut;

    private static Date dateAtMillis(int offset) {
        return new Date(1646000000000L + offset);
    }

    private static Message aMessage(int latency) {
        var message = new Message();
        message.emittedAt = dateAtMillis(0);
        message.ackAt = dateAtMillis(latency);
        return message;
    }

    private static Message aMessage(long latency) {
        return aMessage(Math.toIntExact(latency));
    }

    public static double getKiloBytesFromList(Collection<?> list) throws IOException {
        return getBytesFromList(list) / 1024.;
    }

    public static long getBytesFromList(Collection<?> list) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(baos);
        out.writeObject(list);
        out.close();
        return baos.toByteArray().length;
    }

    @BeforeEach
    void setupSut() {
        sut = new MetricCollector();
    }

    @Test
    void should_add_latency() {
        assertThat(sut.latencies).hasSize(0);

        sut.handleAck(aMessage(10));
        assertThat(sut.latencies).hasSize(1).containsExactly(10L);

        sut.handleAck(aMessage(20));
        assertThat(sut.latencies).hasSize(2).containsExactly(10L, 20L);
    }

    @Test
    void should_print_metrics() {
        List<Long> lts = Arrays.asList(10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L);
        for (var l : lts) {
            sut.handleAck(aMessage(l));
        }

        var metric = sut.printMetrics();

        assertThat(metric.latencyAvg).isEqualTo(55);
        assertThat(metric.latencyPcl50).isEqualTo(percentile(50, lts));
        assertThat(metric.latencyPcl85).isEqualTo(percentile(85, lts));
        assertThat(metric.latencyPcl90).isEqualTo(percentile(90, lts));
        assertThat(metric.latencyPcl95).isEqualTo(percentile(95, lts));
        assertThat(metric.latencyPcl99).isEqualTo(percentile(99, lts));

        assertThat(metric.latencyMin).isEqualTo(10);
        assertThat(metric.latencyMax).isEqualTo(100);

        assertThat(metric.toString()).matches("^\\{.*\"latencyPcl90\":90.*}$");
    }

    @Test
    void should_print_trp() {
        List<Long> lts = Arrays.asList(10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L);
        for (var l : lts) {
            sut.handleAck(aMessage(l));
        }
        sut.lastPrint = Date.from(now().minusSeconds(1));

        var metric = sut.printMetrics();

        assertThat(metric.throughputPerSecond).isEqualTo(10);
    }

    @ParameterizedTest
    @CsvSource({
            //  count    memory-kb
            "100,         1.4",
            "1_000,      13.0",
            "10_000,    130.0",
            "100_000,  1300.0",
    })
    void check_size_of_list(int listSizeCount, double memorySizeKb) throws Exception {
        int precisionPct = 10;

        for (int i = 0; i < listSizeCount; i++) {
            sut.latencies.add(RANDOM.nextLong());
        }

        assertThat(getKiloBytesFromList(sut.latencies))
                .isCloseTo(memorySizeKb, withPercentage(precisionPct));
    }

    /**
     * see: https://stackoverflow.com/a/62315412/10843655
     */
    @Nested
    public class PercentileTest {
        @Test
        public void should_get_percentile() {
            List<Long> list = Arrays.asList(0L, 1L, 2L, 3L);
            assertThat(percentile(51, list)).isEqualTo(2L);
            assertThat(percentile(49, list)).isEqualTo(1L);
        }
    }
}
