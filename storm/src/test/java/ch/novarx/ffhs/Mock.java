package ch.novarx.ffhs;

import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.app.TextGenerator;
import ch.novarx.ffhs.bolt.CountBolt;
import ch.novarx.ffhs.bolt.NoopCommitBolt;
import ch.novarx.ffhs.bolt.SplitBolt;
import ch.novarx.ffhs.operation.NoopFunction;
import ch.novarx.ffhs.spout.TextSpout;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Tuple;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Collections.synchronizedList;

public interface Mock {
    List<Message> getMessages();

    interface MockWithMessages extends Mock {
        void resetMock();
    }

    class TextSpoutM extends TextSpout implements MockWithMessages {
        public static final AtomicBoolean isProcessing = new AtomicBoolean(false);
        private static final List<Message> EXECUTED_VALUES = synchronizedList(new ArrayList<>());

        public TextSpoutM() {
            super();
            TextSpout.QUEUE = new ArrayDeque<>();
            INTERVAL = 10;
        }

        public List<Message> getMessages() {
            return List.copyOf(EXECUTED_VALUES);
        }

        public void resetMock() {
            EXECUTED_VALUES.clear();
            messages.clear();
        }

        public void addToQueue(Message value) {
            TextSpout.QUEUE.add(value);
        }

        public void addToQueue(String value) {
            TextSpout.QUEUE.add(new Message().withText(value));
        }

        public void addToQueue() {
            TextSpout.QUEUE.add(new Message().withText(TextGenerator.generateText()));
        }

        @Override
        protected void emit(Message message) {
            logger.debug("emit", message);
            EXECUTED_VALUES.add(message);
            super.emit(message);
        }

        @Override
        public void nextTuple() {
            isProcessing.set(true);
            super.nextTuple();
        }

        @Override
        protected TimerTask getGenerateTextTask() {
            return new TimerTask() {
                @Override
                public void run() {

                }
            };
        }
    }

    class SplitBoltM extends SplitBolt implements Mock {
        private static final List<Message> EXECUTED_VALUES = synchronizedList(new ArrayList<>());

        public List<Message> getMessages() {
            return List.copyOf(EXECUTED_VALUES);
        }

        @Override
        public void execute(Tuple tuple) {
            logger.debug("execute", tuple);
            EXECUTED_VALUES.add(Message.of(tuple));
            super.execute(tuple);
        }
    }

    class CountBoltM extends CountBolt implements MockWithMessages {
        private static final List<Message> EXECUTED_VALUES = synchronizedList(new ArrayList<>());
        private static NextMessage NEXT_FAILURE = new NextMessage(null, 0);

        public CountBoltM() {
            super(0);
        }

        public List<Message> getMessages() {
            return List.copyOf(EXECUTED_VALUES);
        }

        public void resetMock() {
            EXECUTED_VALUES.clear();
        }

        private synchronized void addMessage(Message message) {
            logger.debug("addValue", message);
            EXECUTED_VALUES.add(message);
        }

        public synchronized void failNext(String value, int times) {
            NEXT_FAILURE = new NextMessage(value, times);
        }

        @Override
        public synchronized void execute(Tuple tuple) {
            Message message = Message.of(tuple);
            addMessage(message);
            logger.debug("execute.msgId", message.word, tuple.getMessageId());
            if (NEXT_FAILURE.shouldBeExecuted(message.word)) {
                logger.debug("execute.failed", message);
                NEXT_FAILURE.times--;
                this.collector.fail(tuple);
            } else {
                logger.debug("execute.do", message);
                super.execute(tuple);
            }
        }

        private static class NextMessage {
            private final String value;
            private int times;

            NextMessage(String value, int times) {
                this.value = value;
                this.times = times;
            }

            public boolean shouldBeExecuted(String word) {
                return times > 0 && value != null && value.equals(word);
            }
        }
    }

    class NoopCommitBoltM extends NoopCommitBolt implements MockWithMessages {
        private static final Deque<Message> EXECUTED_VALUES = new ConcurrentLinkedDeque<>();

        public NoopCommitBoltM() {
            super(0);
        }

        public List<Message> getMessages() {
            return List.copyOf(EXECUTED_VALUES);
        }

        public void resetMock() {
            EXECUTED_VALUES.clear();
        }

        @Override
        public void execute(Tuple tuple) {
            EXECUTED_VALUES.add(Message.of(tuple));
            super.execute(tuple);
            logger.debug("execute", tuple);
        }
    }

    class NoopFunctionM extends NoopFunction implements MockWithMessages {
        private static final List<Message> EXECUTED_VALUES = new ArrayList<>();

        public NoopFunctionM() {
            super(0);
        }

        public List<Message> getMessages() {
            return List.copyOf(EXECUTED_VALUES);
        }

        public void resetMock() {
            EXECUTED_VALUES.clear();
        }

        @Override
        public void execute(TridentTuple tuple, TridentCollector collector) {
            EXECUTED_VALUES.add(Message.of(tuple));
            super.execute(tuple, collector);
        }
    }
}
