package ch.novarx.ffhs.topology;

import ch.novarx.ffhs.FieldName;
import ch.novarx.ffhs.app.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static ch.novarx.ffhs.FieldName.COUNT;
import static ch.novarx.ffhs.FieldName.WORD;
import static org.assertj.core.api.Assertions.assertThat;

class WordCountSealTest extends TopoTestBase {
    protected static final TestClusterSeal sut = new TestClusterSeal();

    @Override
    protected TestClusterBase getSut() {
        return sut;
    }

    public static class TestClusterSeal extends TestClusterBase {
        public TestClusterSeal() {
            DO_DEBUG = false;

            commitMock = commitBolt;
            components.commitBolt = commitBolt;
            components.textSpout = textSpout;
            components.splitBolt = splitBolt;
            components.countBolt = countBolt;

            super.setupTopology(
                    WordCountSeal.builder(components).createTopology()
            );
        }
    }

    @Nested
    @Disabled
    class ReplayTest {
        @BeforeEach
        void resetMocks() {
            sut.textSpout.resetMock();
            sut.countBolt.resetMock();
            sut.commitBolt.resetMock();
        }

        @Test
        void should_emit_text_again_on_failiure() {
            sut.countBolt.failNext("fail", 2);
            sut.textSpout.addToQueue("will fail");
            wait.forSatisfy(s -> assertThat(sut.textSpout.getMessages())
                    .extracting(FieldName.TEXT)
                    .containsExactlyInAnyOrder(
                            "will fail", // initial
                            "will fail", // fail #1
                            "will fail"  // fail #2
                    ));

            // wait to verify no more messages getting emitted by spout
            wait.forViolation(s -> assertThat(sut.textSpout.getMessages()).hasSize(3));
        }

        @Test
        void should_commit_same_hash_for_replayed_text() {
            sut.countBolt.failNext("fail", 2);
            sut.textSpout.addToQueue("will fail");

            wait.forSatisfy(s -> assertThat(sut.commitBolt.getMessages())
                    .anySatisfy(message -> {
                        assertThat(message.count).isEqualTo(1);
                        assertThat(message.word).isEqualTo("fail");
                    }));

            wait.forViolation(s -> {
                String firstHash = sut.commitBolt.getMessages().get(0).hash;
                assertThat(sut.commitBolt.getMessages()).allSatisfy(message -> assertThat(message.hash).isEqualTo(firstHash));
                assertThat(sut.commitBolt.getMessages())
                        .filteredOn("word", "will")
                        .allSatisfy(message -> assertThat(message.count).isEqualTo(1));
                assertThat(sut.commitBolt.getMessages())
                        .filteredOn("word", "fail")
                        .anySatisfy(message -> assertThat(message.count).isEqualTo(1))
                        .allSatisfy(message -> assertThat(message.count).isLessThanOrEqualTo(1));
            });
        }

        @Test
        @Disabled
        void should_commit_correct_count_three_words() {
            sut.countBolt.failNext("fail", 1);
            sut.textSpout.addToQueue("will fail that");

            wait.forSatisfy(s -> assertThat(sut.commitBolt.getMessages())
                    .anySatisfy(message -> {
                        assertThat(message.count).isEqualTo(1);
                        assertThat(message.word).isEqualTo("will");
                    }).anySatisfy(message -> {
                        assertThat(message.count).isEqualTo(1);
                        assertThat(message.word).isEqualTo("fail");
                    }).anySatisfy(message -> {
                        assertThat(message.count).isEqualTo(1);
                        assertThat(message.word).isEqualTo("that");
                    }));

            wait.forViolation(s -> assertThat(sut.commitBolt.getMessages()).filteredOn(m -> m.count > 0)
                    .allSatisfy(message -> assertThat(message.count).isEqualTo(1)));
        }

        @Test
        void should_commit_correct_count_three_words_with_repetition() {
            sut.countBolt.failNext("fail", 1);
            sut.textSpout.addToQueue("will fail that will fail");

            wait.forSatisfy(s -> assertThat(sut.commitBolt.getMessages())
                    .anySatisfy(message -> {
                        assertThat(message.count).isEqualTo(2);
                        assertThat(message.word).isEqualTo("will");
                    }).anySatisfy(message -> {
                        assertThat(message.count).isEqualTo(2);
                        assertThat(message.word).isEqualTo("fail");
                    }).anySatisfy(message -> {
                        assertThat(message.count).isEqualTo(1);
                        assertThat(message.word).isEqualTo("that");
                    }));

            wait.forViolation(s -> {
                assertThat(sut.commitBolt.getMessages())
                        .filteredOn(m -> m.count > 1)
                        .allSatisfy(message -> assertThat(message.count).isEqualTo(2))
                        .allSatisfy(message -> assertThat(message.word).isNotEqualTo("that"));
                assertThat(sut.commitBolt.getMessages())
                        .filteredOn(WORD, "that")
                        .filteredOn(COUNT, 1)
                        .anySatisfy(message -> assertThat(message.word).isEqualTo("that"));
            });
        }
    }

    @Nested
    @Disabled
    class SealTest {
        @BeforeEach
        void resetMocks() {
            sut.textSpout.resetMock();
            sut.countBolt.resetMock();
            sut.commitBolt.resetMock();
        }

        @Test
        void splitter_should_emit_ending_seal() {
            sut.textSpout.addToQueue("lorem");
            wait.forSatisfy(s -> assertThat(sut.countBolt.getMessages()).extracting(WORD).containsExactlyInAnyOrder(
                    "lorem",
                    "|SEAL|",
                    "|SEAL|"
            ));
            wait.forViolation(s -> assertThat(sut.countBolt.getMessages()).extracting(WORD).containsExactlyInAnyOrder(
                    "lorem",
                    "|SEAL|",
                    "|SEAL|"
            ));
        }
    }

    @Nested
    @Disabled
    class TimingTest {

        @BeforeEach
        void resetMocks() {
            sut.textSpout.resetMock();
            sut.countBolt.resetMock();
            sut.commitBolt.resetMock();
            resetLoggerMock();
        }

        @Test
        void should_log_message_id() {
            var message = new Message().withText("lorem");
            sut.textSpout.addToQueue(message);

            wait.forSatisfy(s -> assertThat(getInfoLogs("TextSpoutM::emit")).anySatisfy(log -> {
                assertThat(log).contains("{ \"text\": \"lorem\" }");
                assertThat(log).contains(
                        "||3400bb495c3f8c4c3483a44c6bc1a92e9d94406db75a6f27dbccc11c76450d8a}}");
            }));
        }

        @Test
        void should_ack_message_only_once() {
            var message = new Message().withText("lorem");
            sut.countBolt.failNext("lorem", 1);
            sut.textSpout.addToQueue(message);

            wait.forSatisfyThenViolation(s -> assertThat(getInfoLogs("TextSpoutM::ack"))
                    .hasSize(1)
                    .anySatisfy(log -> assertThat(log).contains(
                            "||3400bb495c3f8c4c3483a44c6bc1a92e9d94406db75a6f27dbccc11c76450d8a}}")));
        }

        @ParameterizedTest
//        @CsvSource({"100", "200"})
        @CsvSource({"10"})
        void should_log_messages_to_measure_performance(int count) {
            for (int i = 0; i < count; i++) {
                sut.textSpout.addToQueue();
            }
            wait.forSatisfy(s -> {
                assertThat(getInfoLogs("TextSpoutM::emit"))
                        .hasSizeBetween(count - 5, count + 5);
                assertThat(getInfoLogs("TextSpoutM::ack"))
                        .hasSizeBetween(count - 5, count + 5);
            });
        }
    }
}
