package ch.novarx.ffhs.topology;

import ch.novarx.ffhs.Components;
import ch.novarx.ffhs.Components.StormConfig;
import org.apache.storm.generated.StormTopology;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class BaseTopoTest extends BaseTopo {
    private static Components.StormConfig sut(String... args) {
        return BaseTopo.argsToConfig(args);
    }

    @Test
    void should_have_default_config() {
        var config = sut();
        assertThat(config.replicas).isEqualTo(1);
        assertThat(config.replicaTextSpout).isEqualTo(0);
        assertThat(config.replicaCountBolt).isEqualTo(0);
    }

    @Test
    void should_parse_arguments() {
        var config = sut(
                "{\"replicaTextSpout\": 10}"
        );
        assertThat(config.replicaTextSpout).isEqualTo(10);
    }

    @Test
    void should_be_validatable() {
        var config = sut(
                "{\"workload\": 0, \"failEveryNth\": 0, \"replicaTextSpout\": 4, \"replicaSplitBolt\": 4, " +
                        "\"replicaCountBolt\": 8, \"replicaCommitBolt\": 8}"
        );
        assertThat(config.validated()).isEqualTo(config);
    }

    @Test
    void should_be_invalid__workload() {
        assertThatThrownBy(() -> sut(
                        "{" +
                                "\"replicaTextSpout\": 2," +
                                "\"workload\": 3" +
                                "}"
                )
        ).hasMessageContaining("invalid replicas or workload");
    }

    @Test
    void should_serialize() {
        var config = new StormConfig();
        config.workload = 1;
        config.failEveryNth = 1;
        config.replicaTextSpout = 1;
        config.replicaSplitBolt = 1;
        config.replicaCountBolt = 1;
        config.replicaCommitBolt = 1;

        var json = config.toString();
        System.out.println(json);
        assertThat(sut(json)).usingRecursiveComparison().isEqualTo(config);
    }

    @Override
    protected StormTopology buildTopology(Components components) {
        return null;
    }
}
