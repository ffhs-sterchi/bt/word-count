package ch.novarx.ffhs.topology;

import ch.novarx.ffhs.Mock.TextSpoutM;
import ch.novarx.ffhs.WaitForTopology;
import ch.novarx.ffhs.utils.Logger;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.message.SimpleMessageFactory;
import org.apache.logging.log4j.simple.SimpleLogger;
import org.apache.logging.log4j.util.PropertiesUtil;
import org.apache.logging.slf4j.Log4jLogger;
import org.junit.jupiter.api.*;

import java.util.Properties;
import java.util.stream.Stream;

import static ch.novarx.ffhs.topology.TestClusterBase.TOPOLOGY_NAME;
import static ch.novarx.ffhs.topology.TestClusterBase.cluster;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestClassOrder(ClassOrderer.OrderAnnotation.class)
public abstract class TopoTestBase {
    protected final WaitForTopology wait = new WaitForTopology(getSut().cluster, TOPOLOGY_NAME);

    @BeforeAll
    static void setupLoggerMock() {
        Logger.LOGGER = spy(new TestLogger());
        doCallRealMethod().when(Logger.LOGGER).debug(anyString());
//        doNothing().when(Logger.LOGGER).info(anyString());
//        doNothing().when(Logger.LOGGER).debug(anyString());
    }

    @AfterAll
    static void tearDown() throws Exception {
        cluster().close();
    }

    protected abstract TestClusterBase getSut();

    void resetLoggerMock() {
        setupLoggerMock();
    }

    protected Stream<String> getLogs(String regex) {
        return mockingDetails(Logger.LOGGER).getInvocations().stream()
                .map(i -> i.getArgumentAt(0, String.class))
                .filter(i -> i.matches(format(".*%s.*", regex)));
    }

    protected Stream<String> getInfoLogs(String regex) {
        return mockingDetails(Logger.LOGGER).getInvocations().stream()
                .filter(i -> i.getMethod().getName().equals("info"))
                .map(i -> i.getArgumentAt(0, String.class))
                .filter(i -> i.matches(format(".*%s.*", regex)));
    }

    protected static class TestLogger extends Log4jLogger {

        public TestLogger() {
            super(new SimpleLogger("TestLogger",
                    Level.DEBUG,
                    false,
                    true,
                    false,
                    false,
                    "HH:mm",
                    SimpleMessageFactory.INSTANCE,
                    new PropertiesUtil(new Properties()),
                    System.out
            ), "TestLogger");
        }
    }

    @Nested
    @Order(1)
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class InitialisationTest {
        @Test
        @Order(1)
        void should_start_workers() throws Exception {
            wait.forCondition(summary -> summary.get_num_workers() > 0);

            int numTasks = getSut().cluster.getTopologySummaryByName(TOPOLOGY_NAME).get_num_tasks();
            assertThat(numTasks).isGreaterThan(1);

            wait.forSatisfy(s -> assertThat(TextSpoutM.isProcessing).isTrue(), 10);
        }

        @Test
        @Order(2)
        void should_emit_text() {
            getSut().textSpout.addToQueue("lorem ipsum");
            wait.forSatisfy(summary -> assertThat(getLogs("TextSpout::emit")).hasSizeGreaterThan(0), 10);
        }

        @Test
        @Order(3)
        void should_execute_commit() {
            wait.forSatisfy(summary -> assertThat(getLogs("TextSpout::ack")).hasSizeGreaterThan(0), 10);
            wait.forSatisfy(summary -> assertThat(getLogs("Noop\\w+::execute")).hasSizeGreaterThan(0),
                    10);
            wait.forSatisfy(summary -> assertThat(getLogs(
                    "Noop\\w+::execute.*lorem"
            )).hasSizeGreaterThan(0));
        }

        @Test
        @Order(4)
        void should_print_stats() {
            wait.forSatisfy(summary -> assertThat(getInfoLogs("MetricCollector::printMetrics")).hasSizeGreaterThan(0), 10);
        }
    }
}
