package ch.novarx.ffhs.topology;

import org.apache.storm.thrift.TException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import static ch.novarx.ffhs.topology.TestClusterBase.TOPOLOGY_NAME;
import static org.assertj.core.api.Assertions.assertThat;

class WordCountTridentTest extends TopoTestBase {
    public static final int WORKLOAD = 10;
    private static final TestClusterTrident sut = new TestClusterTrident();

    @Override
    protected TestClusterBase getSut() {
        return sut;
    }

    public static class TestClusterTrident extends TestClusterBase {
        public static final int FAIL_EVERY_NTH = 10;

        public TestClusterTrident() {
            DO_DEBUG = false;

            components.textSpout = textSpout;
            components.commitFunc = commitFunc;
            commitMock = commitFunc;

            components.config.replicaTextSpout = 1;
            components.config.failEveryNth = FAIL_EVERY_NTH;

            super.setupTopology(
                    new WordCountTrident().buildTopology(components)
            );
        }
    }

    @Nested
    @Disabled
    @Order(5)
    class IntervalTest {

        @Test
        void should_commit_no_null_word() {
            wait.forSatisfyThenViolation(summary -> assertThat(getLogs(
                    "(CountFunction|CountAggregator)::init.*word....null"
            )).hasSizeLessThanOrEqualTo(0));
            wait.forSatisfyThenViolation(summary -> assertThat(getLogs(
                    "NoopFunction::execute.*word....null"
            )).hasSizeLessThanOrEqualTo(0));
        }

        @Test
        void should_fail_some_words() {
            wait.forSatisfyThenViolation(summary -> assertThat(getLogs(
                    "(CountFunction|CountAggregator)::init.*word....null"
            )).hasSizeLessThanOrEqualTo(0));
            wait.forSatisfyThenViolation(summary -> assertThat(getLogs(
                    "NoopFunction::execute.*word....null"
            )).hasSizeLessThanOrEqualTo(0));
        }

        @Test
        @Disabled
        void should_have_bolts() throws TException {
            var id = getSut().cluster.getTopologyInfoByName(TOPOLOGY_NAME).get_id();
            var topology = getSut().cluster.getTopology(id);
            var bolts = topology.get_bolts();

            assertThat(bolts).containsOnlyKeys(
                    "__system",
                    "__acker",
                    "$spoutcoord-spout-text-spout",
                    "spout-text-spout",
                    "b-0-noop-function",
                    "b-1-split-function",
                    "b-2-split-function-count-bolt"
//                "b-2-noop-function",
            );
            wait.forCondition(summary -> summary.get_num_workers() > 15);
        }

        @Test
        @Disabled
        void should_emit_in_correct_interval() throws TException {
            var topology = sut.cluster.getClusterInfo()
                    .get_topologies().stream()
                    .findFirst().orElseThrow();

//            wait.forSatisfy(s -> {
//                assertThat(getLogs("startTimer")).isNotEmpty();
//            });
            var initializedAt = topology.get_uptime_secs();
            wait.forSatisfy(s -> assertThat(getInfoLogs("TextSpoutM::emit")).hasSizeGreaterThan(WORKLOAD * 2));

            int sec = topology.get_uptime_secs() - initializedAt;
            int expectedEmits = sec * WORKLOAD;
            var emits = getInfoLogs("TextSpoutM::emit");

            assertThat(emits).hasSizeBetween(expectedEmits, expectedEmits);
        }
    }
}
