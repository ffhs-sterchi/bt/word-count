package ch.novarx.ffhs.topology;

import ch.novarx.ffhs.Components;
import ch.novarx.ffhs.Mock.*;
import com.google.gson.Gson;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.thrift.TException;

public abstract class TestClusterBase {
    public static final String TOPOLOGY_NAME = "LocalTopo";
    public static final int MESSAGE_TIMEOUT_SECS = 2;
    public static boolean DO_DEBUG = false;
    public final LocalCluster cluster = cluster();
    public final NoopCommitBoltM commitBolt = new NoopCommitBoltM();
    public final NoopFunctionM commitFunc = new NoopFunctionM();
    public final SplitBoltM splitBolt = new SplitBoltM();
    public final CountBoltM countBolt = new CountBoltM();
    protected final Components components = new Components();
    public MockWithMessages commitMock;
    public TextSpoutM textSpout;

    public TestClusterBase() {
        textSpout = new TextSpoutM();
    }

    protected static LocalCluster cluster() {
        try {
            return new LocalCluster();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected static Config config() {
        var json = "{\n" +
                "  \"workload\": 1000,\n" +
                "  \"failEveryNth\": 10000,\n" +
                "  \"replicas\": 2,\n" +
                "  \"replicaTextSpout\": 2,\n" +
                "  \"replicaSplitBolt\": 2,\n" +
                "  \"replicaCountBolt\": 2,\n" +
                "  \"replicaCommitBolt\": 2,\n" +
                "  \"topoConfig\": {\n" +
                "    \"topology.workers\": 20,\n" +
                "    \"topology.max.task.parallelism\": 5,\n" +
                "    \"topology.max.spout.pending\": 100,\n" +
                "    \"topology.spout.max.batch.size\": 100,\n" +
                "    \"topology.trident.batch.emit.interval.millis\": 1,\n" +
                "    \"topology.message.timeout.secs\": 10\n" +
                "  }\n" +
                "}\n";

        Config config = new Gson().fromJson(json, Config.class);

        config.setNumAckers(1);
        config.setDebug(DO_DEBUG);
        return config;
    }

    protected TestClusterBase setupTopology(StormTopology topology) {
        try {
            cluster.submitTopology(TOPOLOGY_NAME, config(), topology);
        } catch (TException e) {
            throw new RuntimeException(e);
        }
        return this;
    }
}
