package ch.novarx.ffhs.spout;

import ch.novarx.ffhs.Components;
import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.app.WaitFor;
import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.spout.SpoutOutputCollector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Date;
import java.util.stream.Collectors;

import static ch.novarx.ffhs.topology.BaseTopo.argsToConfig;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class TextSpoutTest {

    TextSpout sut;
    SpoutOutputCollector collector;

    @BeforeEach
    void setupMock() {
        setupMock(0);
    }

    void setupMock(int workload) {
        if (sut != null) {
            if (sut.timer != null) {
                sut.timer.cancel();
                sut.timer.purge();
            }
        }
        sut = new TextSpout(workload);
        collector = mock(SpoutOutputCollector.class);
        sut.open(null, null, collector);
        if (workload == 0) {
            TextSpout.QUEUE = new ArrayDeque<>();
        }
        Logger.LOG_LEVEL_DEBUG = true;
        reset(sut.collector);
    }

    @Test
    void should_create() {
        assertThat(sut).isNotNull();
    }

    @Nested
    public class MessageHandlingTest {
        Message MESSAGE = aMessage("lorem");

        @Test
        void shouldDoNothingOnEmptyQueue() {
            reset(sut.collector);
            TextSpout.QUEUE.clear();
            sut.nextTuple();
            verifyZeroInteractions(sut.collector);
        }

        @Test
        void shouldEmitMessage() {
            assertThat(TextSpout.QUEUE).isEmpty();
            TextSpout.QUEUE.add(MESSAGE);
            sut.nextTuple();
            assertThat(TextSpout.QUEUE).isEmpty();
            verify(sut.collector).emit(eq(MESSAGE.asValues()), eq(MESSAGE.getId()));
        }

        @Test
        void should_handle_fail() {
            assertThat(TextSpout.QUEUE).isEmpty();
            TextSpout.QUEUE.add(MESSAGE);
            assertThat(TextSpout.QUEUE).containsExactly(MESSAGE);
            sut.nextTuple();
            assertThat(TextSpout.QUEUE).isEmpty();
            sut.fail(MESSAGE.getId());
            assertThat(TextSpout.QUEUE).containsExactly(MESSAGE);

            verify(sut.collector).emit(eq(MESSAGE.asValues()), eq(MESSAGE.getId()));
        }

        @Test
        void should_add_failed_to_front_handle_fail() {
            Message otherMessage = aMessage("other");

            TextSpout.QUEUE.add(MESSAGE);
            TextSpout.QUEUE.add(otherMessage);

            sut.nextTuple();
            sut.fail(MESSAGE.getId());
            sut.nextTuple();

            verify(sut.collector, times(2)).emit(eq(MESSAGE.asValues()), eq(MESSAGE.getId()));
        }

        @Test
        void shouldLogEmittedMessageId() {
            Logger.LOGGER = spy(LoggerFactory.getLogger(Logger.class));
            TextSpout.QUEUE.addFirst(MESSAGE);

            sut.nextTuple();

            verify(Logger.LOGGER).debug(contains(MESSAGE.getId()));
        }

        @Test
        void shouldLogMessageIdOnAck() {
            Logger.LOGGER = spy(LoggerFactory.getLogger(Logger.class));
            sut.messages.put(MESSAGE.getId(), MESSAGE);

            sut.ack(MESSAGE.getId());

            verify(Logger.LOGGER).debug(contains(MESSAGE.getId()));
        }

        @Test
        void should_remove_message_on_ack() {
            TextSpout.QUEUE.add(MESSAGE);
            assertThat(sut.messages).isEmpty();

            sut.nextTuple();
            assertThat(sut.messages).hasSize(1);
            sut.ack(MESSAGE.getId());

            assertThat(sut.messages).isEmpty();
        }

        @Disabled
        void should_add_to_queue_in_interval() {
            setupMock(10);
            sut = spy(sut);
            sut.open(null, null, mock(SpoutOutputCollector.class));
            sut.activate();
            new WaitFor().forSeconds(5);
            sut.timer.cancel();
            sut.timer.purge();

            var inv = mockingDetails(sut).getInvocations().stream()
                    .filter(i -> i.getMethod().getName().equals("addToQueue"))
                    .collect(Collectors.toList());
            assertThat(inv).hasSizeBetween(49, 51);
            assertThat(inv).allSatisfy(i -> {
                var message = i.getArgumentAt(0, Message.class);
                assertThat(message).isNotNull();
                assertThat(message.text).isNotEmpty();
            });
            assertThat(TextSpout.QUEUE).allSatisfy(message -> {
                assertThat(message).isNotNull();
                assertThat(message.text).isNotNull();
                assertThat(message.text).isNotEmpty();
            });
        }

        private Message aMessage(String text) {
            Message message = new Message().withText(text);
            message.emittedAt = Date.from(now().minusMillis(100));
            return message;
        }
    }

    @Nested
    public class FromJsonConfigTest {
        private final String[] args = {"{\n" +
                "  \"workload\": 400,\n" +
                "  \"failEveryNth\": 10000,\n" +
                "  \"replicas\": 4,\n" +
                "  \"replicaTextSpout\": 4,\n" +
                "  \"replicaSplitBolt\": 4,\n" +
                "  \"replicaCountBolt\": 4,\n" +
                "  \"replicaCommitBolt\": 4,\n" +
                "  \"topoConfig\": {\n" +
                "  \"topology.workers\": 20,\n" +
                "  \"topology.max.task.parallelism\": 5,\n" +
                "  \"topology.max.spout.pending\": 100,\n" +
                "  \"topology.spout.max.batch.size\": 100,\n" +
                "  \"topology.trident.batch.emit.interval.millis\": 1,\n" +
                "  \"topology.message.timeout.secs\": 10\n" +
                "}\n" +
                "}"};

        @Test
        void should_create() {
            var components = new Components(argsToConfig(args));
            TextSpout textSpout = (TextSpout) components.textSpout;
            assertThat(textSpout.textPerSecond).isEqualTo(400 / 4);
        }

        @Test
        void should_emit_text_in_interval() {
            var components = new Components(argsToConfig(args));
            var sut = (TextSpout) components.textSpout;
        }
    }
}
