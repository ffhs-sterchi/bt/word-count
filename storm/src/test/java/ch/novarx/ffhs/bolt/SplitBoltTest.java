package ch.novarx.ffhs.bolt;

import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.app.WordSplitter;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.testing.MkTupleParam;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static ch.novarx.ffhs.FieldName.*;
import static java.util.stream.Collectors.toList;
import static org.apache.storm.Testing.testTuple;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class SplitBoltTest {
    SplitBolt sut;

    ArgumentCaptor<Values> valuesCaptor = ArgumentCaptor.forClass(Values.class);

    @BeforeEach
    void setupMock() {
        sut = new SplitBolt();
        sut.splitter = spy(new WordSplitter());
        sut.collector = mock(OutputCollector.class);
    }

    @Test
    void should_create() {
        assertThat(sut).isNotNull();
    }

    @Test
    void should_invoke_WordSplitter() {
        var text = "lorem ipsum";
        var incomingTuple = aTextTuple(text);

        sut.execute(incomingTuple);

        verify(sut.splitter).split(text);
    }

    @Test
    void should_emit_single_words() {
        var incomingTuple = aTextTuple("lorem ipsum");
        sut.execute(incomingTuple);

        verify(sut.collector, atLeast(2))
                .emit(eq(incomingTuple), valuesCaptor.capture());
        verify(sut.collector).ack(incomingTuple);

        List<String> invokedParams = getCapturedValues(WORD);
        assertThat(invokedParams).contains("lorem", "ipsum");
    }

    @Test
    void should_emit_seal() {
        var incomingTuple = aTextTuple("lorem ipsum");
        sut.execute(incomingTuple);

        verify(sut.collector, times(3))
                .emit(eq(incomingTuple), valuesCaptor.capture());
        verify(sut.collector).ack(incomingTuple);

        List<String> invokedParams = getCapturedValues(SEAL);
        assertThat(invokedParams).contains("true");
    }

    private List<String> getCapturedValues(String field) {
        return valuesCaptor.getAllValues().stream()
                .map(v -> String.valueOf(v.get(indexOf(field))))
                .collect(toList());
    }

    private Tuple aTextTuple(String text) {
        MkTupleParam params = new MkTupleParam();
        params.setFields(_ALL);
        return testTuple(new Message().withText(text).asValues(), params);
    }
}
