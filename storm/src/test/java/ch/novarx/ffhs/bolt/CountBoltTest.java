package ch.novarx.ffhs.bolt;

import ch.novarx.ffhs.FieldName;
import ch.novarx.ffhs.app.Message;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.testing.MkTupleParam;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.apache.storm.Testing.testTuple;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class CountBoltTest {
    private static final String HASH1 = "aaa";

    CountBolt sut;

    ArgumentCaptor<Values> valuesCaptor = ArgumentCaptor.forClass(Values.class);

    @BeforeEach
    void setupMock() {
        sut = new CountBolt(0);
        sut.prepare(null, null, mock(OutputCollector.class));
    }

    @AfterEach
    void verifyMocks() {
        verifyNoMoreInteractions(sut.collector);
    }

    @Test
    void should_create() {
        assertThat(sut).isNotNull();
    }

    @Test
    void should_not_emit_count_values_without_seal() {
        var incomingTuple = executeWord();
        verify(sut.collector).ack(incomingTuple);
    }

    @Test
    void should_emit_count_values_after_seal() {
        var incomingTuple = executeWord();
        var seal = executeSeal();

        verify(sut.collector).emit(any(Tuple.class), valuesCaptor.capture());
        verify(sut.collector).ack(incomingTuple);
        verify(sut.collector).ack(seal);

        List<Message> invokedParams = getCapturedMessages();
        assertThat(invokedParams).hasSize(1).allSatisfy(message -> {
            assertThat(message.word).isEqualTo("lorem");
            assertThat(message.count).isEqualTo(1);
        });
    }

    @Test
    void should_emit_count_values_after_seal_with_correct_count() {
        executeWords("lorem", "ipsum", "lorem");
        executeSeal();

        verify(sut.collector, times(4)).ack(any(Tuple.class));
        verify(sut.collector, times(2)).emit(any(Tuple.class), valuesCaptor.capture());
        List<Message> invokedParams = getCapturedMessages();

        assertThat(invokedParams).hasSize(2);

        assertThat(invokedParams).anySatisfy(message -> {
            assertThat(message.word).isEqualTo("lorem");
            assertThat(message.count).isEqualTo(2);
        });

        assertThat(invokedParams).anySatisfy(message -> {
            assertThat(message.word).isEqualTo("ipsum");
            assertThat(message.count).isEqualTo(1);
        });
    }

    @Test
    void should_emit_count_values_only_once_on_replay() {
        executeWordAndSeal();
        executeWordAndSeal();

        verify(sut.collector, times(4)).ack(any(Tuple.class));
        verify(sut.collector, times(2)).emit(any(Tuple.class), valuesCaptor.capture());
        List<Message> invokedParams = getCapturedMessages();

        assertThat(invokedParams).hasSize(2);
        assertThat(invokedParams).allSatisfy(message -> {
            assertThat(message.word).isEqualTo("lorem");
            assertThat(message.count).isEqualTo(1);
        });
    }

    private Tuple executeSeal() {
        var seal = aTuple(new Message().withWord(null).withHash(CountBoltTest.HASH1).withSeal(true));
        sut.execute(seal);
        return seal;
    }

    private Tuple executeWord() {
        return executeWords("lorem").stream().findFirst().orElseThrow();
    }

    private void executeWordAndSeal() {
        executeWords("lorem").stream().findFirst().orElseThrow();
        executeSeal();
    }

    private List<Tuple> executeWords(String... words) {
        var tuples = stream(words).map(word -> aTuple(
                new Message().withWord(word).withHash(CountBoltTest.HASH1).withSeal(false)
        )).collect(toList());
        tuples.forEach(sut::execute);
        return tuples;
    }

    private List<Message> getCapturedMessages() {
        return valuesCaptor.getAllValues().stream()
                .map(Message::of)
                .collect(toList());
    }

    private Tuple aTuple(Message message) {
        MkTupleParam params = new MkTupleParam();
        params.setFields(FieldName._ALL);
        return testTuple(message.asValues(), params);
    }
}
