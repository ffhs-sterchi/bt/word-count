package ch.novarx.ffhs;

import org.apache.storm.LocalCluster;
import org.apache.storm.generated.TopologySummary;
import org.apache.storm.thrift.TException;
import org.apache.storm.utils.Utils;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class WaitForTopology {
    public static final int TIMEOUT_S = 2;

    private final LocalCluster cluster;
    private final String topologyName;

    public WaitForTopology(LocalCluster cluster, String topologyName) {
        this.cluster = cluster;
        this.topologyName = topologyName;
    }

    public void forCondition(Predicate<TopologySummary> condition) throws TException {
        forCondition(condition, TIMEOUT_S);
    }

    public void forCondition(Predicate<TopologySummary> condition, int timeoutS) throws TException {
        forCondition(condition, timeoutS, 0);
    }

    public void forCondition(Predicate<TopologySummary> condition, int timeoutS, int i) throws TException {
        var matched = condition.test(topologySummary());
        if (!matched && i < timeoutS * 2) {
            Utils.sleep(500);
            forCondition(condition, timeoutS, ++i);
        }
    }

    public void forSatisfyThenViolation(Consumer<TopologySummary> requirement) {
        forSatisfy(requirement, TIMEOUT_S, 0);
        forViolation(requirement, TIMEOUT_S, 0);
    }

    public void forSatisfy(Consumer<TopologySummary> requirement) {
        forSatisfy(requirement, TIMEOUT_S);
    }

    public void forSatisfy(Consumer<TopologySummary> requirement, int timeoutS) {
        forSatisfy(requirement, timeoutS, 0);
    }

    public void forSatisfy(Consumer<TopologySummary> requirement, int timeoutS, int i) {
        try {
            requirement.accept(topologySummary());
        } catch (AssertionError e) {
            if (i < timeoutS * 2) {
                Utils.sleep(500);
                forSatisfy(requirement, timeoutS, ++i);
            } else {
                throw e;
            }
        }
    }

    public void forViolation(Consumer<TopologySummary> requirement) {
        forViolation(requirement, TIMEOUT_S);
    }

    public void forViolation(Consumer<TopologySummary> requirement, int timeoutS) {
        forViolation(requirement, timeoutS, 0);
    }

    public void forViolation(Consumer<TopologySummary> requirement, int timeoutS, int i) {
        requirement.accept(topologySummary());
        if (i < timeoutS * 2) {
            Utils.sleep(500);
            forViolation(requirement, timeoutS, ++i);
        }
    }

    private TopologySummary topologySummary() {
        try {
            return cluster.getTopologySummaryByName(topologyName);
        } catch (TException e) {
            return null;
        }
    }
}
