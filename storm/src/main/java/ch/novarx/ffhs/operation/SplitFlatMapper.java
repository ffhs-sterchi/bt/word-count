package ch.novarx.ffhs.operation;

import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.app.WordSplitter;
import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.trident.operation.FlatMapFunction;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;

import static java.util.stream.Collectors.toList;

public class SplitFlatMapper implements FlatMapFunction {
    public static final String NAME = "split-function";

    protected static WordSplitter splitter = new WordSplitter();
    protected static Logger logger = new Logger(SplitFlatMapper.class);

    @Override
    public Iterable<Values> execute(TridentTuple tuple) {
        Message incomming = Message.of(tuple);
        return splitter.split(incomming.text).stream().map(Values::new).collect(toList());
    }
}
