package ch.novarx.ffhs.operation;

import ch.novarx.ffhs.app.Failer;
import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.Function;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.operation.TridentOperationContext;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.ITuple;

import java.util.Map;

/**
 * Used for Trident Topology
 * <p>
 * Fails the batch in the given {@code errorRate}
 *
 * @see NoopFunction#execute
 * @see Failer#shouldFail(ITuple)
 * @see TridentCollector#reportError(java.lang.Throwable)
 */
public class NoopFunction extends BaseFunction implements Function {
    public static final String NAME = "noop-function";

    protected static final Logger logger = new Logger(NoopFunction.class);
    private final int errorRate;

    public NoopFunction(int errorRate) {
        this.errorRate = errorRate;
    }

    @Override
    public void execute(TridentTuple tuple, TridentCollector collector) {
        var message = Message.of(tuple);
        logger.debug("execute", errorRate, message);
        if (errorRate > 0 && Failer.INSTANCE.shouldFail(tuple)) {
            Failer.INSTANCE.doFail(message);
        }
    }

    @Override
    public void prepare(Map<String, Object> conf, TridentOperationContext context) {
        logger.info("prepare", errorRate, errorRate);
    }
}
