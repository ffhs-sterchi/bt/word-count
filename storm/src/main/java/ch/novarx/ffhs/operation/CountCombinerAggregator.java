package ch.novarx.ffhs.operation;

import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.trident.operation.builtin.Count;
import org.apache.storm.trident.tuple.TridentTuple;

/**
 * Used for Trident Topology
 */
public class CountCombinerAggregator extends Count {
    private Logger logger;

    public Logger getLogger() {
        if (logger == null) logger = new Logger(this);
        return logger;
    }

    @Override
    public Long combine(Long val1, Long val2) {
        return val1 + val2;
    }

    @Override
    public Long init(TridentTuple tuple) {
        getLogger().debug("init", Message.of(tuple));
        return 1L;
    }

    @Override
    public Long zero() {
        return super.zero();
    }
}
