package ch.novarx.ffhs.topology;

import ch.novarx.ffhs.Components;
import ch.novarx.ffhs.Components.StormConfig;
import ch.novarx.ffhs.FieldName;
import ch.novarx.ffhs.bolt.CountBolt;
import ch.novarx.ffhs.bolt.NoopCommitBolt;
import ch.novarx.ffhs.bolt.SplitBolt;
import ch.novarx.ffhs.grouping.SealPartialKeyGrouping;
import ch.novarx.ffhs.spout.TextSpout;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

/**
 * Coordinates by Seals/Watermarks
 *
 * @see SplitBolt#execute(Tuple)
 */
public class WordCountSeal extends BaseTopo {
    public static final String NAME = "WordCountSeal";

    public static void main(String[] args) throws Exception {
        BaseTopo.main(NAME, new WordCountSeal(), args);
    }

    public static TopologyBuilder builder(Components components) {
        TopologyBuilder builder = new TopologyBuilder();
        StormConfig c = components.config;

        builder.setSpout(TextSpout.NAME, components.textSpout, c.getReplicasOr(c.replicaTextSpout))
                .setNumTasks(c.getReplicasOr(c.replicaTextSpout));

        builder.setBolt(SplitBolt.NAME, components.splitBolt, c.getReplicasOr(c.replicaSplitBolt))
                .shuffleGrouping(TextSpout.NAME)
                .setNumTasks(c.getReplicasOr(c.replicaSplitBolt));

        builder.setBolt(CountBolt.NAME, components.countBolt, c.getReplicasOr(c.replicaCountBolt))
                .setNumTasks(c.getReplicasOr(c.replicaCountBolt))
                .customGrouping(SplitBolt.NAME, new SealPartialKeyGrouping(new Fields(FieldName.WORD)));

        builder.setBolt(NoopCommitBolt.NAME, components.commitBolt, c.getReplicasOr(c.replicaCommitBolt))
                .setNumTasks(c.getReplicasOr(c.replicaCommitBolt))
                .shuffleGrouping(CountBolt.NAME);

        return builder;
    }

    @Override
    protected StormTopology buildTopology(Components components) {
        return builder(components).createTopology();
    }
}
