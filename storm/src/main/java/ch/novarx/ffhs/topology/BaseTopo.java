package ch.novarx.ffhs.topology;

import ch.novarx.ffhs.Components;
import ch.novarx.ffhs.Components.StormConfig;
import com.google.gson.Gson;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.StormTopology;

public abstract class BaseTopo {
    protected static void main(String name, BaseTopo topo, String[] args) throws Exception {
        var components = new Components(argsToConfig(args));
        Config conf = new Config();
        conf.putAll(components.config.topoConfig);

        StormSubmitter.submitTopology(
                extractTopologyName(name, components.config),
                conf,
                topo.buildTopology(components)
        );
    }

    public static StormConfig argsToConfig(String[] args) {
        if (args.length > 1) {
            throw new RuntimeException("invalid argument");
        }
        if (args.length != 1) {
            return new StormConfig();
        }
        StormConfig config = new Gson().fromJson(args[0], StormConfig.class);
        return config.validated();
    }

    private static String extractTopologyName(String name, StormConfig workload) {
        if (workload.name != null && !workload.name.isEmpty()) {
            return name + "_" + workload.name;
        }
        return name + "_wl" + workload;
    }

    protected abstract StormTopology buildTopology(Components components);
}
