package ch.novarx.ffhs.topology;

import ch.novarx.ffhs.Components;
import ch.novarx.ffhs.Components.StormConfig;
import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.operation.CountCombinerAggregator;
import ch.novarx.ffhs.operation.NoopFunction;
import ch.novarx.ffhs.operation.SplitFlatMapper;
import ch.novarx.ffhs.spout.TextSpout;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.trident.Stream;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.tuple.Fields;

import static ch.novarx.ffhs.FieldName.COUNT;
import static ch.novarx.ffhs.FieldName.WORD;


/**
 * Coordinates by Exactly-once message delivery
 * https://storm.apache.org/releases/current/Trident-tutorial.html
 *
 * @see TridentTopology
 */
public class WordCountTrident extends BaseTopo {
    public static final String NAME = "WordCountTrident";

    public static void main(String[] args) throws Exception {
        BaseTopo.main(NAME, new WordCountTrident(), args);
    }

    public StormTopology buildTopology(Components components) {
        var spout = components.textSpout;
        TridentTopology topology = new TridentTopology();
        Message.FIELDS = null;

        // spout
        StormConfig c = components.config;
        Stream stream = topology.newStream(TextSpout.NAME, spout);

        // split
        stream = stream.name("Texts")
                .flatMap(new SplitFlatMapper(), new Fields(WORD))
                .parallelismHint(c.getReplicasOr(c.replicaTextSpout));

        // count
        stream = stream.name("Words")
                .groupBy(new Fields(WORD))
                .aggregate(new CountCombinerAggregator(), new Fields(COUNT))
                .parallelismHint(c.getReplicasOr(c.replicaTextSpout));

        // commit
        NoopFunction function = new NoopFunction(c.failEveryNth);
        stream = stream.shuffle()
                .name("Counts")
                .parallelismHint(c.getReplicasOr(c.replicaTextSpout))
                .each(new Fields(WORD, COUNT), function, new Fields());

        return topology.build();
    }
}
