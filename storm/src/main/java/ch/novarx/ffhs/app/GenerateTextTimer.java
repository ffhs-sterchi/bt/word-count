package ch.novarx.ffhs.app;

import ch.novarx.ffhs.utils.Logger;

import java.util.Date;
import java.util.TimerTask;
import java.util.function.Consumer;

public class GenerateTextTimer extends TimerTask {
    public static final int INTERVAL_MS = 100;
    private static final Logger logger = new Logger(GenerateTextTimer.class);
    private static final TextGenerator generator = new TextGenerator();

    private final Consumer<Message> messageConsumer;
    private int textPerInterval;

    public GenerateTextTimer(int messagesPerSecond, Consumer<Message> messageConsumer) {
        this.messageConsumer = messageConsumer;
        this.setTextPerSecond(messagesPerSecond);
        logger.debug("GenerateTextTimer", messagesPerSecond);
    }

    @Override
    public void run() {
        logger.debug("run", textPerInterval, getTextPerSecond());
        long start = new Date().getTime();
        for (int i = 0; i < textPerInterval; i++) {
            messageConsumer.accept(generator.nextMessage());
        }
        logger.debug("run.took", new Date().getTime() - start);
    }

    public int getTextPerSecond() {
        int intervalPerSecond = 1000 / INTERVAL_MS;
        return textPerInterval * intervalPerSecond;
    }

    public void setTextPerSecond(int textPerSecond) {
        int intervalPerSecond = 1000 / INTERVAL_MS;
        logger.debug("setTextPerSecond", textPerSecond % intervalPerSecond, textPerSecond, intervalPerSecond);
        assert (textPerSecond % intervalPerSecond) == 0;

        int textPerInterval = textPerSecond / intervalPerSecond;
        logger.debug("setTextPerSecond", textPerInterval % intervalPerSecond, textPerSecond, textPerInterval);
        this.textPerInterval = textPerInterval;
    }
}
