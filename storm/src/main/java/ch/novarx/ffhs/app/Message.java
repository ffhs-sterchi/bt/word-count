package ch.novarx.ffhs.app;

import ch.novarx.ffhs.FieldName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.ITuple;
import org.apache.storm.tuple.Values;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.BiFunction;

import static java.lang.String.format;
import static java.lang.String.join;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.apache.commons.lang.RandomStringUtils.random;

public class Message {
    public static Fields FIELDS = new Fields(FieldName._ALL);
    public Date emittedAt;
    public Date ackAt;
    public String text;
    public String word;
    public Boolean seal;
    public String hash;
    public Integer count;
    private Id id;

    public static Fields getFieldsOrDefault(String... fieldNames) {
        return FIELDS != null ? FIELDS : new Fields(fieldNames);
    }

    public static Message of(ITuple tuple) {
        var message = new Message();
        tuple.getFields().forEach(field -> {
            switch (field) {
                case "text":
                    message.withText(String.valueOf(tuple.getValueByField(field)));
                    break;
                case "word":
                    message.withWord(String.valueOf(tuple.getValueByField(field)));
                    break;
                case "hash":
                    message.withHash(String.valueOf(tuple.getValueByField(field)));
                    break;
                case "seal":
                    message.withSeal((Boolean) tuple.getValueByField(field));
                    break;
                case "count":
                    var val = (Number) tuple.getValueByField(field);
                    if (val != null) message.withCount(val.intValue());
                    break;
            }
        });
        return message;
    }

    public static Message of(TridentTuple tuple) {
        var message = new Message();
        tuple.getFields().forEach(field -> {
            switch (field) {
                case "text":
                    message.withText(String.valueOf(tuple.getValueByField(field)));
                    break;
                case "word":
                    message.withWord(String.valueOf(tuple.getValueByField(field)));
                    break;
                case "hash":
                    message.withHash(String.valueOf(tuple.getValueByField(field)));
                    break;
                case "seal":
                    message.withSeal((Boolean) tuple.getValueByField(field));
                    break;
                case "count":
                    message.withCount(Math.toIntExact((Long) tuple.getValueByField(field)));
                    break;
            }
        });
        return message;
    }

    public static Message of(List<Object> tuple) {
        return new Message()
                .withText((String) tuple.get(0))
                .withWord((String) tuple.get(1))
                .withSeal((Boolean) tuple.get(2))
                .withHash((String) tuple.get(3))
                .withCount((Integer) tuple.get(4));
    }

    public Values asValues() {
        return asValues(true);
    }

    public Values asValues(boolean withNonNull) {
        if (withNonNull) return new Values(text, word, seal, hash, count);
        var values = new ArrayList<>();
        if (text != null) values.add(text);
        if (word != null) values.add(word);
        if (seal != null) values.add(seal);
        if (hash != null) values.add(hash);
        if (count != null) values.add(count);
        return new Values(values);
//        return new Values(text, word, seal, hash, count);
    }

    public String getId() {
        return id.toString();
    }

    public Message withText(String text) {
        this.text = text;
        this.id = new Id(this.text);
        return this;
    }

    public Message withWord(String word) {
        this.word = word;
        return this;
    }

    public Message withSeal(Boolean seal) {
        this.seal = seal;
        return this;
    }

    public Message withHash(String hash) {
        this.hash = hash;
        return this;
    }

    public Message withCount(Integer count) {
        this.count = count;
        return this;
    }

    public Long getLatencyMs() {
        return emittedAt != null && ackAt != null ? ackAt.getTime() - emittedAt.getTime() : null;
    }

    @Override
    public String toString() {
        BiFunction<String, Object, String> p = (String label, Object value) ->
                format("\"%s\": \"%s\"", label, value);

        var list = new ArrayList<String>();
        if (text != null) list.add(p.apply("text", text));
        if (word != null) list.add(p.apply("word", word));
        if (seal != null) list.add(p.apply("seal", seal));
        if (hash != null) list.add(p.apply("hash", hash));
        if (count != null) list.add(p.apply("count", count));
        var latencyMs = getLatencyMs();
        if (latencyMs != null) {
            list.add(p.apply("latencyMs", latencyMs.intValue()));
        }

        return format("{ %s }", join(", ", list));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        return new EqualsBuilder().append(text, message.text).append(word, message.word).append(seal,
                message.seal).append(hash, message.hash).append(count, message.count).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(text).append(word).append(seal).append(hash).append(count).toHashCode();
    }

    public static class Id implements Serializable {
        static final long serialVersionUID = 1L;

        private final Date created;
        private String hash;

        public Id(String text) {
            setHash(text);
            this.created = new Date();
        }

        private Id(Date created, String hash) {
            this.hash = hash;
            this.created = created;
        }

        public static Id of(String stringId) {
            var parts = stringId.split("\\|\\|");
            var created = new Date(Long.parseLong(parts[0]));
            var hash = parts[1];
            return new Id(created, hash);
        }

        private void setHash(String text) {
            hash = sha256Hex(text != null ? text : random(10));
        }

        @Override
        public String toString() {
            if (hash == null || hash.isEmpty()) throw new IllegalStateException("hash must be provided!");
            return format("%s||%s", created.getTime(), hash);
        }
    }
}
