package ch.novarx.ffhs.app;

import org.apache.storm.utils.Utils;

import java.util.Date;

public class WaitFor {
    public static final int TIMEOUT_S = 2;

    public void forMilliSeconds(int milliseconds) {
        waitForEnd(milliseconds);
    }

    public void forSeconds(int seconds) {
        waitForEnd(seconds * 1000);
    }

    private void waitForEnd(int timeoutMs) {
        waitForEnd(timeoutMs, new Date().getTime());
    }

    private void waitForEnd(int timeoutMs, long startMilis) {
        int halfTick = 100;

        long passedMs = new Date().getTime() - startMilis;
        long passedAfterMs = passedMs + halfTick;
        if (passedAfterMs < timeoutMs) {
            Utils.sleep(halfTick * 2);
            waitForEnd(timeoutMs, startMilis);
        }
    }

    public void forSatisfy(Runnable requirement) {
        forSatisfy(requirement, TIMEOUT_S);
    }

    public void forSatisfy(Runnable requirement, int timeoutS) {
        forSatisfy(requirement, timeoutS, 0);
    }

    public void forSatisfy(Runnable requirement, int timeoutS, int i) {
        try {
            requirement.run();
        } catch (AssertionError e) {
            if (i < timeoutS * 2) {
                Utils.sleep(500);
                forSatisfy(requirement, timeoutS, ++i);
            } else {
                throw e;
            }
        }
    }

    public void forViolation(Runnable requirement) {
        forViolation(requirement, TIMEOUT_S);
    }

    public void forViolation(Runnable requirement, int timeoutS) {
        forViolation(requirement, timeoutS, 0);
    }

    public void forViolation(Runnable requirement, int timeoutS, int i) {
        requirement.run();
        if (i < timeoutS * 2) {
            Utils.sleep(500);
            forViolation(requirement, timeoutS, ++i);
        }
    }
}
