package ch.novarx.ffhs.app;

import java.util.List;

public class WordSplitter {
    public static final String WORD_SPLIT_PATTERN = "([^a-zA-Z0-9_-]+)";

    public List<String> split(String text) {
        String[] words = text.toLowerCase().split(WORD_SPLIT_PATTERN);
        return List.of(words);
    }
}
