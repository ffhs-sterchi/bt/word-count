package ch.novarx.ffhs.app;

import java.util.Random;

import static ch.novarx.ffhs.app.Dictionary.ENGLISH_WORDS;

public class TextGenerator {
    private static final Random random = new Random();

    public static String generateText() {
        var wordBuilder = new StringBuilder();
        var randomNumberOfWords = random.nextInt(40) + 10;
        for (int i = 0; i < randomNumberOfWords; i++) {
            wordBuilder.append(randomWord());
            wordBuilder.append(" ");
        }
        return wordBuilder.toString().trim();
    }

    public static String randomWord() {
        int index = random.nextInt(ENGLISH_WORDS.size());
        return ENGLISH_WORDS.get(index);
    }

    public String randomText() {
        return generateText();
    }

    public Message nextMessage() {
        return new Message().withText(randomText());
    }
}
