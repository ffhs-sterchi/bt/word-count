package ch.novarx.ffhs.app;

import java.util.ArrayDeque;

/**
 * Returns a TextMessage on every poll
 */
public class AdHocQueue extends ArrayDeque<Message> {
    protected final TextGenerator generator = new TextGenerator();

    @Override
    public Message poll() {
        if (size() <= 0) {
            this.add(generator.nextMessage());
        }
        return super.poll();
    }
}
