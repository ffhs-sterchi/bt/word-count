package ch.novarx.ffhs.app;

import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.topology.FailedException;
import org.apache.storm.tuple.ITuple;
import org.apache.storm.tuple.Tuple;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

public class Failer {
    public static final String THIS_WORD_SHOULD_FAIL = "this_word_should_fail";
    private static final Logger logger = new Logger(Failer.class);
    public static Failer INSTANCE = new Failer();
    private static int nextFail = 0;

    private final Deque<String> failedTuples = new ConcurrentLinkedDeque<>();

    public boolean shouldFailEmittingMessage(int errorRate) {
        boolean should = errorRate > 0 && ++nextFail % errorRate == 0;
        logger.debug("shouldFailAtRate", should, errorRate);
        return should;
    }

    public boolean shouldFail(ITuple tuple) {
        Message message = Message.of(tuple);
        return message.word != null
                && message.word.startsWith(THIS_WORD_SHOULD_FAIL)
                && !failedTuples.contains(message.word);
    }

    public void doFail(Tuple tuple, OutputCollector collector) {
        Message message = Message.of(tuple);
        fail(message, () -> collector.fail(tuple));
    }

    public void doFail(Message message) {
        fail(message, () -> {
            throw new FailedException("Failer::fail");
        });
    }

    private void fail(Message message, Runnable failFunction) {
        if (failedTuples.size() > 20_000) {
            failedTuples.pollLast();
        }

        if (!failedTuples.contains(message.word)) {
            failedTuples.addFirst(message.word);
            logger.infoMessage("doFail", message);
            failFunction.run();
        }
    }
}
