package ch.novarx.ffhs;

import ch.novarx.ffhs.app.Failer;
import ch.novarx.ffhs.bolt.CountBolt;
import ch.novarx.ffhs.bolt.NoopCommitBolt;
import ch.novarx.ffhs.bolt.SplitBolt;
import ch.novarx.ffhs.spout.TextSpout;
import com.google.gson.Gson;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.trident.operation.BaseFunction;

import java.util.HashMap;
import java.util.Map;

public class Components {
    public IRichSpout textSpout;
    public IRichBolt splitBolt = new SplitBolt();
    public IRichBolt countBolt;
    public IRichBolt commitBolt;
    public BaseFunction commitFunc;

    public StormConfig config;

    public Components(StormConfig config) {
        this.config = config;
        int workload = config.workload / config.getReplicasOr(config.replicaTextSpout);
        Failer.INSTANCE = new Failer();

        textSpout = new TextSpout(workload, config.failEveryNth);

        countBolt = new CountBolt(0);
        commitBolt = new NoopCommitBolt(config.failEveryNth);
    }

    public Components() {
        this(new StormConfig());
    }

    public static class StormConfig {
        public String name = "";

        public int workload = 10;
        public Integer replicas = 1;
        public Integer failEveryNth = 0;
        public Integer replicaTextSpout = 0;
        public Integer replicaSplitBolt = 0;
        public Integer replicaCountBolt = 0;
        public Integer replicaCommitBolt = 0;


        /**
         * @implNote topology.max.spout.pending
         * @implNote topology.max.task.parallelism
         * @implNote topology.message.timeout.secs
         * @implNote topology.spout.max.batch.size
         * @implNote topology.trident.batch.emit.interval.millis
         * How often a new batch is emitted
         * @implNote topology.workers
         * the number of executors (threads) can change over time
         * @implNote topology.tasks
         * number of tasks is the same throughout the lifetime of a topology
         */
        public Map<String, Integer> topoConfig = new HashMap<>();

        public StormConfig validated() {
            if (workload % replicaTextSpout != 0) throw new RuntimeException("invalid replicas or workload");
            return this;
        }

        public int getReplicasOr(int defaultValue) {
            return this.replicas > 0 ? this.replicas : defaultValue;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }
}
