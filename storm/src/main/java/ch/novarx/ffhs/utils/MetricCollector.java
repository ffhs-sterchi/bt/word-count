package ch.novarx.ffhs.utils;

import ch.novarx.ffhs.app.Message;
import com.google.gson.Gson;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;

import static java.lang.Math.round;

public class MetricCollector {
    private static final long INTERVAL_MS = 10_000;
    public static boolean START_TIMER = true;
    public final Deque<Long> latencies = new ConcurrentLinkedDeque<>();
    private final Logger logger = new Logger(this);
    public Date lastPrint;
    protected Timer timer = new Timer();

    public MetricCollector() {
        if (START_TIMER) startTimer();
    }

    /**
     * from: https://stackoverflow.com/a/62315412/10843655
     */
    public static Long percentile(double percentile, List<Long> items) {
        Collections.sort(items);
        return percentileForSorted(percentile, items);
    }

    private static Long percentileForSorted(double percentile, List<Long> items) {
        if (percentile <= 0 || percentile >= 100 || items.isEmpty()) {
            return null;
        }
        return items.get((int) round(percentile / 100.0 * (items.size() - 1)));
    }

    public void handleAck(Message message) {
        latencies.add(message.getLatencyMs());
    }

    public Metric printMetrics() {
        var size = latencies.size();
        var date = new Date();
        var duration = lastPrint != null ? date.getTime() - lastPrint.getTime() : null;
        lastPrint = date;

        List<Long> all = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            all.add(latencies.pollFirst());
        }
        var metric = new Metric(all, duration);
        logger.info("printMetrics", metric);
        return metric;
    }

    protected void startTimer() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                printMetrics();
            }
        }, INTERVAL_MS, INTERVAL_MS);
    }

    public static class Metric {
        public Integer count;
        public Long durationMs;
        public Double throughputPerSecond;
        public Double throughputPerMinute;

        public Double latencyAvg;
        public Long latencyMax;
        public Long latencyMin;

        public Long latencyPcl50;
        public Long latencyPcl85;
        public Long latencyPcl90;
        public Long latencyPcl95;
        public Long latencyPcl99;

        public Metric(List<Long> all, Long durationMs) {
            count = all.size();
            this.durationMs = durationMs;

            if (count > 0) {
                if (durationMs != null) {
                    throughputPerSecond = count / (durationMs / 1000.);
                    throughputPerMinute = throughputPerSecond * 60;
                }

                OptionalDouble average = all.stream().mapToDouble(a -> a).average();
                latencyAvg = average.isPresent() ? average.getAsDouble() : null;

                Collections.sort(all);
                latencyPcl50 = percentileForSorted(50, all);
                latencyPcl85 = percentileForSorted(85, all);
                latencyPcl90 = percentileForSorted(90, all);
                latencyPcl95 = percentileForSorted(95, all);
                latencyPcl99 = percentileForSorted(99, all);

                latencyMax = all.get(all.size() - 1);
                latencyMin = all.get(0);
            }
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }
}
