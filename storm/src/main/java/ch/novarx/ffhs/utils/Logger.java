package ch.novarx.ffhs.utils;


import ch.novarx.ffhs.app.Message;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class Logger {
    public static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Logger.class);

    public static boolean LOG_LEVEL_DEBUG = true;
    private final String classname;

    public Logger(Object forObject) {
        this.classname = forObject.getClass().getSimpleName();
    }

    public Logger(Class<?> cls) {
        this.classname = cls.getSimpleName();
    }

    public void debug(String method, Object... text) {
        if (!LOG_LEVEL_DEBUG) return;
        var logText = builder(method)
                .append("[")
                .append(Arrays.toString(text))
                .append("]");
        LOGGER.debug(logText.toString());
    }

    public void debug(String method, Message message) {
        if (!LOG_LEVEL_DEBUG) return;
        var logText = builder(method)
                .append("(")
                .append(message)
                .append(")");
        LOGGER.debug(logText.toString());
    }

    private StringBuilder message() {
        return new StringBuilder()
                .append("##BT## ")
                .append(classname)
                .append(" ## ");
    }

    private StringBuilder builder(String method) {
        return new StringBuilder()
                .append("##BT## ")
                .append(classname)
                .append("::")
                .append(method)
                .append(" ## ");
    }

    public void info(String method, Object... text) {
        var logText = builder(method)
                .append("[")
                .append(Arrays.toString(text))
                .append("]");
        LOGGER.info(logText.toString());
    }

    public void info(String method, Message message) {
        var logText = builder(method)
                .append("(")
                .append(message)
                .append(")");
        LOGGER.info(logText.toString());
    }

    public void info(String method, Message message, String messageId) {
        var logText = builder(method)
                .append("(")
                .append(messageId)
                .append("), ")
                .append("(")
                .append(message)
                .append(")");
        LOGGER.info(logText.toString());
    }

    public void infoMessage(String method, Object messageId) {
        infoMessage(method, String.valueOf(messageId), null);
    }

    public void infoMessage(String method, String messageId, Message message) {
        var logText = builder(method)
                .append("(")
                .append("{{")
                .append(messageId)
                .append("}}")
                .append(" ")
                .append(message)
                .append(")");
        LOGGER.info(logText.toString());
    }
}
