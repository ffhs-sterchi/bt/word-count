package ch.novarx.ffhs;

abstract public class FieldName {
    public static final String MESSAGE = "text";
    public static final String TEXT = "text";
    public static final String WORD = "word";
    public static final String SEAL = "seal";
    public static final String HASH = "hash";
    public static final String COUNT = "count";
    public static final String[] _ALL = {TEXT, WORD, SEAL, HASH, COUNT};

    public static int indexOf(String field) {
        int i = 0;
        for (; i < _ALL.length; i++) {
            if (_ALL[i].equals(field)) break;
        }
        return i;
    }
}
