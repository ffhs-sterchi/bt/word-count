package ch.novarx.ffhs.grouping;

import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.generated.GlobalStreamId;
import org.apache.storm.grouping.PartialKeyGrouping;
import org.apache.storm.task.WorkerTopologyContext;
import org.apache.storm.tuple.Fields;

import java.util.List;

public class SealPartialKeyGrouping extends PartialKeyGrouping {
    protected Logger logger;
    private List<Integer> allTargetTasks;

    public SealPartialKeyGrouping(Fields fields) {
        super(fields);
    }

    @Override
    public void prepare(WorkerTopologyContext context, GlobalStreamId stream, List<Integer> targetTasks) {
        logger = new Logger(this);
        this.allTargetTasks = targetTasks;
        logger.info("prepare", allTargetTasks);
        super.prepare(context, stream, targetTasks);
    }

    @Override
    public List<Integer> chooseTasks(int taskId, List<Object> values) {
        var message = Message.of(values);
        if (message.seal) {
            return allTargetTasks;
        }
        return super.chooseTasks(taskId, values);
    }
}
