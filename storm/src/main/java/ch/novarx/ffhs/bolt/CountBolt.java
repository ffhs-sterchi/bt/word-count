package ch.novarx.ffhs.bolt;

import ch.novarx.ffhs.app.Failer;
import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.HashMap;
import java.util.Map;

import static ch.novarx.ffhs.FieldName.COUNT;
import static ch.novarx.ffhs.FieldName.WORD;
import static java.util.Collections.emptyMap;

public class CountBolt extends BaseRichBolt {
    public static final String NAME = "count-bolt";

    public final Integer errorRate;
    protected OutputCollector collector;
    protected Logger logger;
    private Map<String, Map<String, Integer>> hashToWords;

    public CountBolt(Integer errorRate) {
        this.errorRate = errorRate;
    }

    @Override
    public void prepare(Map<String, Object> conf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.hashToWords = new HashMap<>();
        logger = new Logger(this);
    }

    @Override
    public void execute(Tuple tuple) {
        Message incomming = Message.of(tuple);
        logger.debug("execute", incomming);

        if (incomming.seal) {
            this.hashToWords.getOrDefault(incomming.hash, emptyMap())
                    .forEach((word, count) -> {
                        Message message = new Message()
                                .withWord(word)
                                .withCount(count)
                                .withHash(incomming.hash);
                        collector.emit(tuple, message.asValues());
                        this.hashToWords.get(incomming.hash).put(word, 0);
                    });
            this.hashToWords.remove(incomming.hash);
        } else {
            if (errorRate > 0 && Failer.INSTANCE.shouldFail(tuple)) {
                this.hashToWords.remove(incomming.hash);
                Failer.INSTANCE.doFail(tuple, collector);
            }
            this.hashToWords.putIfAbsent(incomming.hash, new HashMap<>());
            this.hashToWords.get(incomming.hash).putIfAbsent(incomming.word, 0);
            incrementCountFor(incomming.hash, incomming.word);
        }

        collector.ack(tuple);
    }

    private synchronized void incrementCountFor(String hash, String word) {
        Integer integer = this.hashToWords.get(hash).get(word);
        this.hashToWords.get(hash).put(word, integer + 1);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(Message.getFieldsOrDefault(WORD, COUNT));
    }
}
