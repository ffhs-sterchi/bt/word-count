package ch.novarx.ffhs.bolt;

import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.app.WordSplitter;
import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.Map;

import static ch.novarx.ffhs.FieldName.*;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

public class SplitBolt extends BaseRichBolt {
    public static final String NAME = "split-bolt";
    protected Logger logger;
    OutputCollector collector;
    WordSplitter splitter;

    @Override
    public void prepare(Map<String, Object> conf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        splitter = new WordSplitter();
        logger = new Logger(this);
    }

    @Override
    public void execute(Tuple tuple) {
        Message incomming = Message.of(tuple);
        String hash = sha256Hex(incomming.text);

        var splitted = splitter.split(incomming.text);
        splitted.forEach(word -> collector.emit(
                tuple,
                new Message()
                        .withHash(hash)
                        .withSeal(false)
                        .withWord(word)
                        .asValues()
        ));
        collector.emit( // seal
                tuple,
                new Message().withHash(hash).withSeal(true).withWord("|SEAL|").asValues()
        );
        collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(Message.getFieldsOrDefault(WORD, SEAL, HASH));
    }
}
