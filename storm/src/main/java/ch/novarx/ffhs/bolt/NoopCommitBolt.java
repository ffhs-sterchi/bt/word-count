package ch.novarx.ffhs.bolt;

import ch.novarx.ffhs.app.Failer;
import ch.novarx.ffhs.utils.Logger;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.Map;

public class NoopCommitBolt extends BaseRichBolt implements IRichBolt {
    public static final String NAME = "commit-bolt";
    private final int errorRate;
    protected OutputCollector collector;
    protected Logger logger;

    public NoopCommitBolt(int errorRate) {
        this.errorRate = errorRate;
    }

    protected Logger logger() {
        return new Logger(this);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }

    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        logger = new Logger(this);
    }

    @Override
    public void execute(Tuple tuple) {
        if (errorRate > 0 && Failer.INSTANCE.shouldFail(tuple)) {
            Failer.INSTANCE.doFail(tuple, collector);
        }
        collector.ack(tuple);
    }
}
