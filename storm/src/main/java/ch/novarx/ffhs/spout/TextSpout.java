package ch.novarx.ffhs.spout;

import ch.novarx.ffhs.app.AdHocQueue;
import ch.novarx.ffhs.app.Failer;
import ch.novarx.ffhs.app.GenerateTextTimer;
import ch.novarx.ffhs.app.Message;
import ch.novarx.ffhs.utils.Logger;
import ch.novarx.ffhs.utils.MetricCollector;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.utils.Utils;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static ch.novarx.ffhs.FieldName.TEXT;
import static ch.novarx.ffhs.app.Failer.THIS_WORD_SHOULD_FAIL;
import static ch.novarx.ffhs.app.GenerateTextTimer.INTERVAL_MS;
import static java.lang.Math.toIntExact;
import static org.apache.storm.utils.Utils.secureRandomLong;


public class TextSpout extends BaseRichSpout {
    public static final String NAME = "text-spout";
    protected static final Logger logger = new Logger(TextSpout.class);
    protected static int INTERVAL = 1;
    protected static Deque<Message> QUEUE = new ArrayDeque<>();
    public final int textPerSecond;
    public final int maxQueueSize = 50_000;
    protected final Map<String, Message> messages = new HashMap<>();
    private final int errorRate;
    public MetricCollector metrics;
    protected Timer timer;
    protected SpoutOutputCollector collector;
    AtomicInteger latencyAcked = new AtomicInteger(0);
    AtomicInteger countAcked = new AtomicInteger(0);

    public TextSpout(int workload) {
        textPerSecond = workload;
        errorRate = 0;
    }

    public TextSpout(int workload, int errorRate) {
        textPerSecond = workload;
        this.errorRate = errorRate;
    }

    public TextSpout() {
        textPerSecond = 10;
        errorRate = 0;
    }

    public synchronized void addToQueue(Message message) {
        if (QUEUE.size() < maxQueueSize) {
            QUEUE.add(message);
        }
    }

    @Override
    public void nextTuple() {
        var message = QUEUE.poll();
        if (message == null) {
            Utils.sleep(INTERVAL);
            return;
        }

        if (Failer.INSTANCE.shouldFailEmittingMessage(errorRate)) {
            message.withText(message.text + " " + THIS_WORD_SHOULD_FAIL + secureRandomLong());
        }

        logger.debug("nextTuple", message);
        messages.put(message.getId(), message);
        emit(message);
    }

    protected void emit(Message message) {
        logger.debug("emit", message.getId(), message);
        message.emittedAt = message.emittedAt == null ? new Date() : message.emittedAt;
        collector.emit(message.asValues(Message.getFieldsOrDefault(TEXT).size() > 1), message.getId());
    }

    protected TimerTask getGenerateTextTask() {
        return new GenerateTextTimer(textPerSecond, this::addToQueue);
    }

    @Override
    public void ack(Object msgId) {
        Message message = messages.remove((String) msgId);
        logger.debug("ack", message.getId(), message);
        message.ackAt = new Date();
        ackStat(message);
        metrics.handleAck(message);
    }

    public synchronized void ackStat(Message message) {
        var latencySum = latencyAcked.addAndGet(toIntExact(message.getLatencyMs()));
        int count = countAcked.incrementAndGet();
        if (count >= 1000) {
            logger.info("ackStat", count, latencySum / count);
            countAcked.set(0);
            latencyAcked.set(0);
        }
    }

    @Override
    public void fail(Object msgId) {
        Message failedMessage = messages.get((String) msgId);
        logger.infoMessage("fail", failedMessage.getId(), failedMessage);
        QUEUE.addFirst(failedMessage);
    }

    @Override
    public void open(Map<String, Object> conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        metrics = new MetricCollector();
        if (textPerSecond <= 0) {
            QUEUE = new AdHocQueue();
        } else {
            timer = new Timer();
            timer.scheduleAtFixedRate(getGenerateTextTask(), INTERVAL_MS, INTERVAL_MS);
        }
    }

    @Override
    public void activate() {
    }

    @Override
    public void deactivate() {
        if (timer != null) timer.purge();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(
                Message.getFieldsOrDefault(TEXT)
        );
    }
}
