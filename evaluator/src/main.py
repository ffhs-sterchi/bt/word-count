import traceback

import flask
from flask import Flask, render_template, request, jsonify, send_from_directory
from flask import json
from werkzeug.exceptions import HTTPException

from service.deployment import writeJsonConfig, readJsonConfig, getLatestPackage, \
    getLatestFile, downloadJarAndGetVersion, writeRun, readRuns, deleteRun, updateRuns
from service.log import logMetrics, logSummary, getLogsAsJsonLines, _LOGS_PATH, logFilenameSummary
from service.storm import getClusterSummary, getTopologies, getTopology, deleteTopology, deployStorm, readStormConfig, \
    writeStormConfig

app = Flask(__name__)


@app.route('/', methods=['GET'])
def main():
    topoConfig = readJsonConfig() \
        .replace(", ", ",\n  ") \
        .replace("{", "{\n  ") \
        .replace("}", "\n}")
    stormConfig = readStormConfig()
    return render_template('index.html', topoConfig=topoConfig, stormConfig=stormConfig)


@app.route('/runs', methods=['GET'])
def getRuns():
    return jsonify(readRuns())


@app.route('/runs/<topology>', methods=['POST'])
def addRun(topology: str):
    return jsonify(writeRun(
        topology, request.json
    ))


@app.route('/runs/update/all', methods=['POST'])
def overwriteRuns():
    return jsonify(updateRuns(
        request.json
    ))


@app.route('/runs/<runId>/delete', methods=['DELETE', 'POST'])
def removeRun(runId: str):
    return deleteRun(runId)


@app.route('/metrics')
def putMetric():
    logMetrics(request.json)


@app.route('/logs/<name>')
def getLogsStormDeploy(name):
    return jsonify(
        getLogsAsJsonLines(_LOGS_PATH + name + '.log')
    )


@app.route('/logs')
def getMetricLogfiles():
    return jsonify(
        logFilenameSummary()
    )


@app.route('/package', methods=['GET'])
def latestFile():
    package = getLatestPackage()
    file = getLatestFile(package['id'])
    return {
        'package': package,
        'file': file
    }


@app.route('/storm/supervisor/summary', methods=['GET'])
def stormSummary():
    return getClusterSummary()


@app.route('/storm/topology/summary', methods=['GET'])
def stormTopologies():
    return getTopologies()


@app.route('/storm/topology/<name>', methods=['GET'])
def stormTopology(name: str):
    return getTopology(name)


@app.route('/storm/topology/<name>/delete', methods=['DELETE', 'POST'])
def removeTopology(name: str):
    return deleteTopology(name)


@app.route('/deploy/jar/<configId>', methods=['GET'])
def deployJarForRun(configId: str = None):
    version = downloadJarAndGetVersion()
    return deployStorm(configId, jarVersion=version)


@app.route('/deploy/config/topo', methods=['POST'])
def setTopoConfig():
    return writeJsonConfig(
        request.json
    )


@app.route('/deploy/config/storm', methods=['POST'])
def setStormConfig():
    return writeStormConfig(
        request.data.decode("utf-8")
    )


@app.route('/assets/<path:path>')
def sendAssets(path):
    return send_from_directory('assets', path)


@app.errorhandler(HTTPException)
def handle_http_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "message": e.description,
    })
    response.content_type = "application/json"
    return response


@app.errorhandler(Exception)
def handle_exception(e: Exception):
    """Return JSON instead of HTML for HTTP errors."""
    response = flask.Response()
    response.data = json.dumps({
        "code": None,
        "name": e.__str__(),
        "message": traceback.format_exception(type(e), e, e.__traceback__),
    })
    response.status_code = 500
    response.content_type = "application/json"
    return response


logMetrics({})
logSummary("none", {})

if __name__ == "__main__":
    app.config['DEBUG'] = True
    app.run(host='0.0.0.0', port=5000)
