import flask
from flask import json


def aErrorResponse(name: str, message: str, statusCode=500):
    response = flask.Response()
    response.data = json.dumps({
        "name": name,
        "message": message,
    })
    response.status_code = statusCode
    response.content_type = "application/json"
    return response
