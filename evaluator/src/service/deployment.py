#!/usr/bin/env python3
import json
import os
from uuid import uuid4

import requests

from service.log import now

BASE_URL = 'https://gitlab.com/api/v4/projects/32827304'
HEADERS = {
    'PRIVATE-TOKEN': os.getenv('GITLAB_TOKEN'),
    'Accept': '*/*'
}


def getLatestPackage():
    endpoint = '%s/packages?order_by=created_at&sort=desc' % BASE_URL
    response = requests.get(endpoint, headers=HEADERS).json()
    return response[0]


def getLatestFile(packageId: str):
    endpoint = '%s/packages/%s/package_files?order_by=created_at&sort=desc' % (BASE_URL, packageId)
    response = requests.get(endpoint, headers=HEADERS).json()
    return response[0]


def downloadFile(fileId: str):
    endpoint = 'https://gitlab.com/ffhs-sterchi/bt/images/-/package_files/%s/download' % fileId
    response = requests.get(endpoint, stream=True)
    # Throw an error for bad status codes
    response.raise_for_status()

    with open('storm.jar', 'wb') as handle:
        for block in response.iter_content(1024):
            handle.write(block)


def readJsonConfig():
    return open('storage/topo-config.json', 'r').read()


def writeJsonConfig(configJson: dict):
    open('storage/topo-config.json', 'w').write(json.dumps(configJson))
    return open('storage/topo-config.json', 'r').read()


def readRuns():
    runs: list = json.loads(open('storage/runs.json', 'r').read())

    def byCreated(entry):
        if 'created' in entry:
            return entry['created']
        return entry['id']

    runs.sort(key=byCreated)
    return runs


def readRun(runId: str):
    runs: list = readRuns()
    for run in runs:
        if run['id'] == runId:
            return run
    return None


def writeRun(topology: str, configJson: dict):
    runs: list = readRuns()
    id = uuid4().__str__()
    writeJsonConfig(configJson)

    runs.append({
        "id": id,
        "config": configJson,
        "done": False,
        "topology": topology,
        "created": now()
    })

    open('storage/runs.json', 'w').write(json.dumps(runs))
    return readRuns()


def deleteRun(runId: str):
    runs: list = readRuns()
    deleted = {}
    for run in runs:
        if run['id'] == runId:
            deleted = run
            runs.remove(run)
    open('storage/runs.json', 'w').write(json.dumps(runs))
    return deleted


def updateRuns(configJson: dict):
    open('storage/runs.json', 'w').write(json.dumps(configJson))
    return readRuns()


def updateRun(configJson: dict):
    runs: list = readRuns()
    for run in runs:
        if run['id'] == configJson['id']:
            runs.remove(run)
    runs.append(configJson)
    open('storage/runs.json', 'w').write(json.dumps(runs))
    return readRuns()


def downloadJarAndGetVersion():
    package = getLatestPackage()
    file = getLatestFile(package['id'])
    downloadFile(file['id'])
    return package['version']
