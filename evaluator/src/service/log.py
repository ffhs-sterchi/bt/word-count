import glob
import os
import re
from datetime import datetime
from pprint import pprint

from flask import json

_LOGS_PATH = 'storage/'
if os.path.isdir('/logs'):
    _LOGS_PATH = '/logs/'

PATHS = {
    'summary': _LOGS_PATH + 'topology_%s.log',
    'metrics': _LOGS_PATH + 'bt-metrics.log',
    'deploy_storm': _LOGS_PATH + 'deploy_storm.log',
}


def logFilenameSummary():
    filenames = glob.glob(PATHS['summary'] % '*')
    p = re.compile(r"(?P<name>topology_[\w-]+)\.log")

    def mapTopo(filename):
        m = p.search(filename)
        return str(m.group('name'))

    return list(map(mapTopo, filenames))


def now():
    return datetime.now().strftime("%d-%b-%Y_%H:%M:%S")


def logSummary(topologyName: str, summary: dict):
    path = PATHS['summary'] % topologyName
    log(path, summary)


def logMetrics(metrics: dict):
    path = PATHS['metrics']
    log(path, metrics)


def getLogs(path):
    if os.path.isfile(path):
        return open(path, 'w+').read()
    return ""


def getLogsAsJsonLines(path):
    file = open(path, 'r')
    lines = file.readlines()
    logs = []
    for line in lines:
        for item in line.replace("}{", "}}{{").split("}{"):
            jsonItem = json.loads(item.replace("\n", ""))
            logs.append(jsonItem)
    return logs


def log(path: str, data: dict):
    try:
        if not os.path.isfile(path):
            with open(path, 'w+') as file:
                file.writelines([json.dumps(data)])
        with open(path, 'a') as file:
            file.writelines([json.dumps(data)])
    except:
        print("ERROR: cannot write to file under: " + path)
        pprint(data)
