import json
import os
import subprocess

import requests

from service.deployment import readRun, updateRun, readJsonConfig, getLatestPackage
from service.errorHandling import aErrorResponse
from service.log import logSummary, log, now, PATHS

username = 'admin' if os.getenv('STORM_UI_USER') is None else os.getenv('STORM_UI_USER')
password = 'oYcjNds63qMpjsisywrXkVFAy3xi66Ho' if os.getenv('STORM_UI_PASS') is None else os.getenv('STORM_UI_PASS')
host = 'localhost:8580' if os.getenv('STORM_UI_HOST') is None else os.getenv('STORM_UI_HOST')

login = '%s:%s@' % (username, password) if username is None else ''
stormUiUrl = 'http://%s%s/api/v1' % (login, host)

TOPOS = {
    'seal': 'WordCountSeal',
    'trident': 'WordCountTrident',
}

STORM_CONFIG_FILE_PATH = '/conf/storm.yaml'
if not os.path.isdir('/conf'):
    STORM_CONFIG_FILE_PATH = 'storm.yaml'


def readStormConfig():
    print(STORM_CONFIG_FILE_PATH)
    return open(STORM_CONFIG_FILE_PATH, 'r').read()


def writeStormConfig(configYaml: str):
    open(STORM_CONFIG_FILE_PATH, 'w').write(configYaml)
    return open(STORM_CONFIG_FILE_PATH, 'r').read()


def getClusterSummary():
    endpoint = stormUiUrl + '/supervisor/summary'
    response = requests.get(endpoint, timeout=5).json()
    return response


def getTopologies():
    endpoint = stormUiUrl + '/topology/summary'
    response = requests.get(endpoint, timeout=5).json()
    return response


def getTopology(name: str):
    endpoint = stormUiUrl + '/topology/' + name
    response = requests.get(endpoint, timeout=5).json()
    logSummary(name, response)
    return response


def deleteTopology(name: str):
    endpoint = stormUiUrl + '/topology/' + name + '/kill/0'
    response = requests.post(endpoint, timeout=5).json()
    return response


def deployStorm(configId: str = None, jarVersion: str = None):
    run = None
    if configId is None:
        config = readJsonConfig()
        topoType = TOPOS['seal']
    else:
        run = readRun(configId)
        run['config']['name'] = run['id']
        config = json.dumps(run['config'])
        topoType = run['topology']
    stormDeployCommand = [
        "storm",
        "jar",
        "storm.jar",
        "ch.novarx.ffhs.topology.%s" % topoType,
        config.replace("\n", "")
    ]

    log(PATHS['deploy_storm'], {
        'topology': config,
        'storm': readStormConfig(),
        'run': run,
        'jar': getLatestPackage(),
        'created': now(),
    })

    try:
        output = subprocess.check_output(stormDeployCommand, text=True, stderr=subprocess.STDOUT)
        if configId is not None:
            run = readRun(configId)
            run['done'] = True
            run['jarVersion'] = jarVersion
            updateRun(run)
        return output
    except subprocess.CalledProcessError as e:
        return aErrorResponse("command '%s' return with error (code %s): {}" % (e.cmd, e.returncode), e.output)
