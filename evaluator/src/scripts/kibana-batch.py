# Gets a CSV Report from Kibana every x Minutes

import time
from urllib.request import urlretrieve

import requests

INTERVAL_MINUTES = 5

CSV_FILE_PATH = "C:\\dev\\ffhs\\bt-storm-poc\\results\\"
BASE_URL = "http://192.168.2.5:5602"
GENERATE_URL = BASE_URL + "/api/reporting/generate/csv?jobParams=%28conflictedTypesFields%3A%21%28%29%2Cfields%3A%21%28%27%40timestamp%27%2Cbt_topology.keyword%2Cbt_method_name.keyword%2C%27%40timestamp%27%2Cbt_logline.keyword.value_count%2Cbt_metrics.latencyAvg.avg%2Cbt_metrics.latencyMin.min%2Cbt_metrics.latencyMax.max%2Cbt_metrics.latencyPcl99.avg%2Cbt_metrics.latencyPcl95.avg%2Cbt_metrics.latencyPcl90.avg%2Cbt_metrics.latencyPcl50.avg%2Cbt_metrics.throughputPerSecond.min%2Cbt_metrics.throughputPerSecond.max%2Cbt_metrics.throughputPerSecond.avg%2Cbt_metrics.count.sum%29%2CindexPatternId%3Aad31d5d0-9d58-11ec-9646-3f02a48200f7%2CmetaFields%3A%21%28_source%2C_id%2C_type%2C_index%2C_score%29%2CsearchRequest%3A%28body%3A%28_source%3A%28excludes%3A%21%28%29%2Cincludes%3A%21%28%27%40timestamp%27%2Cbt_topology.keyword%2Cbt_method_name.keyword%2Cbt_logline.keyword.value_count%2Cbt_metrics.latencyAvg.avg%2Cbt_metrics.latencyMin.min%2Cbt_metrics.latencyMax.max%2Cbt_metrics.latencyPcl99.avg%2Cbt_metrics.latencyPcl95.avg%2Cbt_metrics.latencyPcl90.avg%2Cbt_metrics.latencyPcl50.avg%2Cbt_metrics.throughputPerSecond.min%2Cbt_metrics.throughputPerSecond.max%2Cbt_metrics.throughputPerSecond.avg%2Cbt_metrics.count.sum%29%29%2Cdocvalue_fields%3A%21%28%28field%3A%27%40timestamp%27%2Cformat%3Adate_time%29%29%2Cquery%3A%28bool%3A%28filter%3A%21%28%28match_all%3A%28%29%29%2C%28range%3A%28%27%40timestamp%27%3A%28format%3Astrict_date_optional_time%2Cgte%3A%272022-01-31T23%3A00%3A00.000Z%27%2Clte%3A%272022-03-31T21%3A59%3A59.999Z%27%29%29%29%29%2Cmust%3A%21%28%29%2Cmust_not%3A%21%28%29%2Cshould%3A%21%28%29%29%29%2Cscript_fields%3A%28%29%2Csort%3A%21%28%28%27%40timestamp%27%3A%28order%3Adesc%2Cunmapped_type%3Aboolean%29%29%29%2Cstored_fields%3A%21%28%27%40timestamp%27%2Cbt_topology.keyword%2Cbt_method_name.keyword%2Cbt_logline.keyword.value_count%2Cbt_metrics.latencyAvg.avg%2Cbt_metrics.latencyMin.min%2Cbt_metrics.latencyMax.max%2Cbt_metrics.latencyPcl99.avg%2Cbt_metrics.latencyPcl95.avg%2Cbt_metrics.latencyPcl90.avg%2Cbt_metrics.latencyPcl50.avg%2Cbt_metrics.throughputPerSecond.min%2Cbt_metrics.throughputPerSecond.max%2Cbt_metrics.throughputPerSecond.avg%2Cbt_metrics.count.sum%29%2Cversion%3A%21t%29%2Cindex%3A%27bt-%2A-metrics%27%29%2Ctitle%3Abt--metrics%2Ctype%3Asearch%29"

LAST_LINECOUNT = 32439


def fetchReport():
    global LAST_LINECOUNT
    x = requests.post(GENERATE_URL, headers={'kbn-xsrf': 'true'})
    jsonResponse = x.json()
    csvUrl = BASE_URL + jsonResponse['path']

    print(csvUrl)

    statusCode = 0
    i = 0
    maxWaitSec = 20
    while statusCode != 200 and i < maxWaitSec:
        i += 1
        print("Status Code was '{}', waiting another {} Seconds...".format(statusCode, maxWaitSec - i))
        time.sleep(1)
        statusCode = requests.head(csvUrl, headers={'kbn-xsrf': 'true'}).status_code
    if statusCode == 200:
        urlretrieve(csvUrl, CSV_FILE_PATH + "bt--metrics.csv")
        file = open(CSV_FILE_PATH + "bt--metrics.csv", "r")
        line_count = 0
        for line in file:
            if line != "\n":
                line_count += 1
        file.close()
        print("Success! Got: {} Lines, means {} new Lines".format(line_count, line_count - LAST_LINECOUNT))
        LAST_LINECOUNT = line_count
    else:
        print("csv was not ready...")


if __name__ == '__main__':
    while True:
        fetchReport()
        print("Do next run in {} Minutes".format(INTERVAL_MINUTES))
        time.sleep(INTERVAL_MINUTES * 60)
        print("______")
