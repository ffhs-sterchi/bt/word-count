# Convert a CSV with all the Run-Configurations to a Matrix
import csv
import json

MAPPING = {
    'ï»¿sig_full': None,  # 'sig_full',
    'short_id': None,  # 'short_id',

    'Average of failEveryNthPerTopology': 'Fehlerrate',
    'Average of workloadPerSpout': 'Arbeitslast',
    'Average of replicasOrCountReplicas': 'Skalierung',
    'Average of max.spout.pending': 'Nebenläufigkeit',
    'Average of max.batch.size': 'Batch-Grösse',
    'Average of trident.batch.emit.interval.millis': 'Batch-Dauer',
}

MAPPING_MATRIX = {
    'Fehlerrate': 'FR',
    'Arbeitslast': 'AL',
    'Skalierung': 'SK',
    'Nebenläufigkeit': 'NL',
    'Batch-Grösse': 'BG',
    'Batch-Dauer': 'BD',
}


def read():
    reader = csv.DictReader(open('params.csv'), delimiter=',')
    allParams = []
    for paramEntry in reader:
        t = {}
        for paramKey in paramEntry:
            mappedKey = MAPPING[paramKey]
            paramValue = paramEntry[paramKey]
            t[mappedKey] = paramValue
        allParams.append(t)
    open('params.json', 'w', encoding='UTF-8').write(json.dumps(allParams, indent=2))
    return allParams


def keyValueKey(entry, paramKey):
    paramValue = int(entry[paramKey])
    paramValue = paramValue if paramValue < 10000 else "{}t".format(int(paramValue / 1000))
    return "\"{}\r\n{}\"".format(MAPPING_MATRIX[paramKey], paramValue)


def toMatrix(allParams):
    matrix = {}
    for paramEntry in allParams:
        for f in paramEntry:
            for t in paramEntry:
                if f is not None and f is not t and t is not None and f is not None:
                    keyFrom = keyValueKey(paramEntry, f)
                    keyTo = keyValueKey(paramEntry, t)

                    row = matrix.get(keyFrom, {})
                    col = int(row.get(keyTo, 0)) + 1

                    row[keyTo] = col
                    matrix[keyFrom] = row
    return matrix


def keyCompare(key: str) -> str:
    splitted = key.replace("\"", "").replace("t", "000").split('\r\n')
    paramKey = splitted[0]
    paramValue = splitted[1]

    return "{}_{:010d}".format(paramKey, int(paramValue))


def matrixToCsv(matrix):
    allKeys = set()
    for f in matrix:
        allKeys.add(f)
        for t in matrix[f]:
            allKeys.add(t)
    allKeys = list(allKeys)
    allKeys.sort(key=keyCompare)
    with open('../../../results/param_matrix.csv', 'w', newline='', encoding='UTF-8') as output_file:
        print("von," + "\t".join(allKeys), file=output_file)
        for f in allKeys:
            row = []
            for t in allKeys:
                fromEntry = matrix.get(f, {})
                toValue = fromEntry.get(t, 0)
                row.append("X" if toValue > 0 else "")
            print("{},{}".format(f, "\t".join(row)), file=output_file)


params = read()
matrix = toMatrix(params)

# pprint(matrix['Arbeitslast 0'])
# print(matrix['Arbeitslast 0']['Skalierung 1'])
matrixToCsv(matrix)
