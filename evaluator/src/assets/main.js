const INTERVAL = 8000;
const INTERVAL_LONG = 30_000;

const STORE = {
    topologies: [],
    runs: [],
    getRunForTopo: topo => STORE.runs?.find(r => topo.id.includes(r.id))
};
// run sorting
const byReplicas = (a, b) => allReplicasMean(a) - allReplicasMean(b);
const byWorkload = (a, b) => a.config.workload - b.config.workload;
const allReplicasMean = run =>
    run.config?.replicas > 0 ?
        run.config.replicas :
        (run.config.replicaCommitBolt
            + run.config.replicaCountBolt
            + run.config.replicaSplitBolt
            + run.config.replicaTextSpout) / 4;


const doGet = (path) => {
    return $.get(path).fail(addMessageLog).then();
}

const doPost = (path) => {
    return $.post(path).fail(addMessageLog).then();
}

function toStorage(element) {
    const value = $(element).text()
    localStorage.setItem('bt-runs', value)
}

function fromStorage() {
    const runs = JSON.parse(localStorage.getItem('bt-runs'));
    console.log(runs);
    $.post({
        url: '/runs/update/all',
        data: JSON.stringify(runs),
        contentType: 'application/json'
    }).done(() => getRuns()).fail(addMessageLog)
}

function copyToClipboard(text) {
    var $temp = $("<textarea>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}

const deleteTopo = id => doPost(`/storm/topology/${id}/delete`)
const deleteRuns = id => doPost(`/runs/${id}/delete`)

const addMessageLog = (err) => {
    const template = $('#deployMessage > div:first-of-type');
    const messageEle = $('#deployMessage');

    const message = err?.responseJSON?.message != null ? err?.responseJSON?.message : err;
    const details = Array.isArray(message) ? message?.join('<br>') : message.replace('\n', '<br>');
    const name = err?.responseJSON?.name != null ? err?.responseJSON?.name : 'Error';
    const date = new Date();

    const content = template.html()
        .replaceAll('[[title]]', `${date.toLocaleTimeString()}: ${name}`)
        .replaceAll('[[text]]', details)
        .replaceAll('[[id]]', Math.floor(Math.random() * 1000000))
    messageEle.html(messageEle.html() + content)
}

const getRuns = () => $.get('/runs').done(runs => {
    STORE.runs = runs;
    render();
}).fail(addMessageLog);

const getPendingTopologies = () => {
    return STORE.runs.filter(t => !t.done).sort(byWorkload).sort(byReplicas);
}

const getTopologies = () => $.get('/storm/topology/summary').done(json => {
    Promise.all(json.topologies.map(topo => getTopology(topo.id))).then(topologies => {
        STORE.topologies = topologies;
        if (STORE.nextRun == null) {
            setNextRun(getPendingTopologies()[0]);
        }
        renderTopologies();
    })
}).fail(addMessageLog);

const totalReplicas = runConfig => (runConfig?.replicaTextSpout || 0) + (runConfig?.replicaSplitBolt || 0) + (runConfig?.replicaCountBolt || 0) + (runConfig?.replicaCommitBolt || 0);
const logTopologyDash = (topo) => {
    const emitter = topo.bolts.find(b => b.boltId === 'spout-text-spout') || topo.spout;
    const workloadPerSecond = Math.round(emitter.emittedPerMinute / 60);
    const run = STORE.getRunForTopo(topo);

    const dash = {
        uptime: topo.uptime,
        uptimeSeconds: topo.uptimeSeconds,
        status: topo.status,
        workersTotal: topo.workersTotal,
        workersRequested: totalReplicas(run?.config),
        workloadPerSecond,
        workloadPerSecondRequested: run?.config?.workload,
        boltLoad: topo.bolts.map(b => Math.round(b.capacity * 100)).map(l => l + '%').join(', ')
    }
    localStorage.setItem('bt_TopologyDash_' + (run?.id || topo.id), JSON.stringify(dash));
    return dash;
}


const emittedPerMinute = (topo, comp) => Math.round(((comp.emitted / topo.uptimeSeconds) * 60))
const ackedPerMinute = (topo, comp) => Math.round(((comp.acked / topo.uptimeSeconds)))

const getTopology = (id) => $.get('/storm/topology/' + id).then(topo => {
    return {
        id,
        uptime: topo.uptime,
        uptimeSeconds: topo.uptimeSeconds,
        topologyStats: topo.topologyStats.map(stat => ({
            acked: stat.acked,
            completeLatency: stat.completeLatency,
            emitted: stat.emitted,
            failed: stat.failed,
            transferred: stat.transferred,
            // window: stat.window,
            windowPretty: stat.windowPretty,
        })).filter(v => v.windowPretty === '10m 0s'),
        status: topo.status,
        spout: topo.spouts.map(spout => ({
            acked: spout.acked,
            completeLatency: spout.completeLatency,
            emitted: spout.emitted,
            perMinute: emittedPerMinute(topo, spout),
            emittedPerMinute: emittedPerMinute(topo, spout),
            ackedPerMinute: ackedPerMinute(topo, spout),
            // encodedSpoutId: spout.encodedSpoutId,
            // errorHost: spout.errorHost,
            // errorLapsedSecs: spout.errorLapsedSecs,
            // errorPort: spout.errorPort,
            // errorTime: spout.errorTime,
            // errorWorkerLogLink: spout.errorWorkerLogLink,
            executors: spout.executors,
            failed: spout.failed,
            // lastError: spout.lastError,
            // requestedCpu: spout.requestedCpu,
            // requestedGenericResourcesComp: spout.requestedGenericResourcesComp,
            // requestedMemOffHeap: spout.requestedMemOffHeap,
            // requestedMemOnHeap: spout.requestedMemOnHeap,
            spoutId: spout.spoutId,
            tasks: spout.tasks,
        }))[0],
        workersTotal: topo.workersTotal,
        bolts: topo.bolts.map(bolt => ({
            acked: bolt.acked,
            boltId: bolt.boltId,
            capacity: bolt.capacity,
            emitted: bolt.emitted,
            perMinute: emittedPerMinute(topo, bolt),
            emittedPerMinute: emittedPerMinute(topo, bolt),
            ackedPerMinute: ackedPerMinute(topo, bolt),
            // encodedBoltId: bolt.encodedBoltId,
            // errorHost: bolt.errorHost,
            // errorLapsedSecs: bolt.errorLapsedSecs,
            // errorPort: bolt.errorPort,
            // errorTime: bolt.errorTime,
            // errorWorkerLogLink: bolt.errorWorkerLogLink,
            executeLatency: bolt.executeLatency,
            // executed: bolt.executed,
            // executors: bolt.executors,
            failed: bolt.failed,
            // lastError: bolt.lastError,
            processLatency: bolt.processLatency,
            // requestedCpu: bolt.requestedCpu,
            // requestedGenericResourcesComp: bolt.requestedGenericResourcesComp,
            // requestedMemOffHeap: bolt.requestedMemOffHeap,
            // requestedMemOnHeap: bolt.requestedMemOnHeap,
            // tasks: bolt.tasks,
            // transferred: bolt.transferred,
        }))
    };
}).fail(addMessageLog);

const getCluster = () => $.get('/storm/supervisor/summary').done(json => {
    const list = json.supervisors.map(supervisor => {
        const disabled = supervisor.blacklisted ? 'disabled' : '';
        return `<li class="${disabled}">${supervisor.host}</li>`
    });
    $('#supervisors').html(`
                        <ul>${list.join('')}</ul>
                    `);
});

const getPackage = () => $.get('/package').done(json => {
    const gitlabUrl = 'https://gitlab.com';
    const gitlabPackageFilePath = gitlabUrl + '/ffhs-sterchi/bt/images/-/package_files/';
    const gitlabPackage = json.package;
    const packageFile = json.file;

    $('#package-info').html(`
                <a href="${gitlabPackageFilePath}${packageFile.id}/download">${packageFile.file_name}</a>
                <a href="${gitlabUrl}${gitlabPackage._links.web_path}">
                    (${gitlabPackage.name}: ${gitlabPackage.version})
                </a>
                `)
}).fail(addMessageLog);

const getLogsStormDeploy = () => doGet('/logs/deploy_storm')
    .then(logs => STORE.logs = logs)
    .then(renderLogLines)

const getLogsMetrics = () => doGet('/logs')
    .then(files => STORE.metricFiles = files)
    .then(renderMetricfiles)

const LS_NEXTONUPTIME = 'bt_nextOnUptime';
const LS_UPTIME_INTERVAL = 'ls_uptime_interval';
const nextOnUptime = async () => {
    const deployInterval = 60 * getOrSetUptimeInterval();
    // if (deployInterval <= 0) return;

    const runningTopologies = () => (STORE?.topologies?.filter(t => t?.status !== 'KILLED') || []);
    const topoToKill = runningTopologies().filter(t => t?.uptimeSeconds >= deployInterval)[0];

    // Kill
    if (deployInterval > 0 && topoToKill != null) {
        console.log('nextOnUptime.kill', topoToKill);
        await deleteTopo(topoToKill.id);
    }

    // Get next topo to deploy
    const nextRun = STORE.nextRun != null ? {...STORE.nextRun} : getPendingTopologies()[0];
    if (nextRun == null) {
        console.warn('no runs to add', STORE.nextRun, getPendingTopologies()[0]);
        return;
    }

    // Skip if already a running topo and next is highly scaled
    const runningTopos = runningTopologies().length;
    const nextHasHighScaling = nextRun?.config?.replicas > 4;

    if (runningTopos > 1) {
        console.warn('there is more than one running topo - skip', nextRun, runningTopologies());
        return;
    }
    if (runningTopos > 0 && nextHasHighScaling) {
        console.warn('there is a running topo and the new run has more than 4 replicas - skip', nextRun, runningTopologies());
        return;
    }

    // Deploy new
    console.log('nextOnUptime.add', nextRun, runningTopologies());
    await doGet(`/deploy/jar/${nextRun.id}`);
    STORE.nextRun = null;
}

const setNextRun = (runId) => {
    STORE.nextRun = STORE.runs.find(r => r.id === runId);
    renderRuns();
}

const renderTopologies = () => {
    const cards = [];
    for (const topo of STORE.topologies) {
        const dash = logTopologyDash(topo);
        const run = STORE.runs?.find(r => topo.id.includes(r.id)) || '';

        const configJson = JSON.stringify(run, null, 2)
            .replaceAll('\n', '<br>')
            .replaceAll('"', "'");
        const fullConfig = `<pre>${configJson}</pre>`;
        const info = `
            <button type="button" class="btn btn-outline-secondary btn-sm" data-container="body" data-toggle="popover" data-placement="right" data-html="true" data-content="${fullConfig}">
              config
            </button>`;
        cards.push(`
                        <div class="col-12 mb-4">
                            <h5>
                                ${topo.id}
                                    ${info}
                                    <button class="btn btn-outline-warning btn-sm" onclick="deleteTopo('${topo.id}')" style="margin-left: 1rem">
                                        Kill
                                    </button>
                            </h5>
                            <span class="mr-5">Uptime: ${topo.uptime}</span>
                            <span class="mr-5">Status: ${topo.status}</span>
                            <span class="mr-5">Workers: ${topo.workersTotal}</span>
                        </div>
                    `)
        cards.push(`
                        <div class="col-4 mb-4">
                            <div><span  style="display: inline-block;min-width: 80px" class="mr-1">Status: </span>
                                ${dash.status}
                            </div>
                            <div><span  style="display: inline-block;min-width: 80px" class="mr-1">Uptime: </span>
                                ${dash.uptime} (${dash.uptimeSeconds})
                            </div>
                        </div>
                        <div class="col-4 mb-4">
                            <div><span  style="display: inline-block;min-width: 80px" class="mr-1">Workers: </span>
                                ${dash.workersTotal} / ${dash.workersRequested}
                            </div>
                            <div><span  style="display: inline-block;min-width: 80px" class="mr-1">Workload: </span>
                                ${dash.workloadPerSecond} / ${dash.workloadPerSecondRequested}
                            </div>
                        </div>
                        <div class="col-4 mb-4">
                            <div><span  style="display: inline-block;min-width: 80px" class="mr-1">Load: </span>
                                ${dash.boltLoad}
                            </div>
                        </div>
                    `)
        cards.push(`
                        <div class="col-4 ">
                            <h5>Stats</h5>
                            <pre>${JSON.stringify(topo.topologyStats, null, 2)}</pre>
                        </div>
                    `)
        cards.push(`
                        <div class="col-4 ">
                            <h5>Spout</h5>
                            <pre>${JSON.stringify(topo.spout, null, 2)}</pre>
                        </div>
                    `)
        cards.push(`
                        <div class="col-4 ">
                            <h5>Bolts</h5>
                            <pre>${JSON.stringify(topo.bolts, null, 2)}</pre>
                        </div>
                    `)
    }
    $('#topologies').html(cards.join('') + '<hr>')
    enablePopovers();
}
const renderRuns = () => {
    const deployButton = line => {
        const text = line.done ? `re-deploy` : `deploy`;
        const type = line.done ? `secondary` : `primary`;
        return `
        <a class="btn btn-outline-${type} btn-sm" onclick="doGet('/deploy/jar/${line.id}')">
            ${text}
        </a>`
    };
    const nextRunButton = line => {
        let isNextRun = STORE.nextRun?.id === line.id;
        const text = isNextRun ? `isNext` : `doNext`;
        const type = isNextRun ? `secondary` : `primary`;
        return `
        <a class="btn btn-outline-${type} btn-sm" onclick="setNextRun('${line.id}')">
            ${text}
        </a>`
    };
    const filterDone = $('#filterDone').is(':checked');

    const lines = STORE.runs.sort(byWorkload).sort(byReplicas).filter(r => !filterDone || r.done === false).map(line => {
        const scalingValues = runConfig => [
            {title: 'TextSpout', value: runConfig.replicas ? runConfig.replicas : runConfig.replicaTextSpout},
            {title: 'SplitBolt', value: runConfig.replicas ? runConfig.replicas : runConfig.replicaSplitBolt},
            {title: 'CountBolt', value: runConfig.replicas ? runConfig.replicas : runConfig.replicaCountBolt},
            {title: 'CommitBolt', value: runConfig.replicas ? runConfig.replicas : runConfig.replicaCommitBolt},
            {
                title: 'Total', value: runConfig.replicas ? runConfig.replicas : totalReplicas(runConfig)
            },
        ];
        const scaling = scalingValues(line.config).map((s, i) => `
                <span class="badge badge-${i < 4 ? 'light' : 'secondary'}" data-toggle="tooltip" data-placement="top" title="${s.title}">
                ${s.value}x
                </span>
            `)
            .join('');

        const configJson = JSON.stringify(line, null, 2)
            .replaceAll('\n', '<br>')
            .replaceAll('"', "'");
        const fullConfig = `<pre>${configJson}</pre>`;

        return `
            <tr>
            <td><span data-toggle="tooltip" data-placement="top" title="${line.id}" onclick="copyToClipboard('${line.id}')">
                ${(line.id + '').slice(0, 3)}
            </span></td>
            <td><span data-bs-toggle="tooltip" data-bs-html="true" title="<em>Tooltip</em> <u>with</u> <b>HTML</b>">
                <button type="button" class="btn btn-outline-secondary btn-sm" data-container="body" data-toggle="popover" data-placement="right" data-html="true" data-content="${fullConfig}">
                  ${line.topology}
                </button>
            </span></td>
            <td>${deployButton(line)} ${nextRunButton(line)}</td>
            <td>${line.config.workload === 0 ? 'unthrottled' : line.config.workload + '/s'}</td>
            <td><sup>1</sup>/<sub>${parseInt(line.config.failEveryNth, 10)}</sub></td>
            <td>${scaling}</td>
            <td><a class="btn btn-outline-warning btn-sm" onclick="deleteRuns('${line.id}')">X</a></td>
            </tr>
            `
    }).join('\n')
    $('#runs').html(lines)
    $('#allTestRuns').text(JSON.stringify(runs))
    $('[data-toggle="tooltip"]').tooltip()
    enablePopovers();
}
const renderRunsMetadata = () => {
    const lines = STORE.runs
        .map(run => {
            let template = $("#templateMetadataRow").html();
            for (const runKey in run.config) {
                template = template.replaceAll(`[[${runKey}]]`, run.config[runKey])
            }
            return template.replaceAll('[[id]]', run.id)
        })
        .join('')

    $('#metaLines').html(lines)
}
const copyMetadata = () => {
    const asText = $('#metaLines')
        .html()
        .replaceAll('\n', '')
        .replaceAll(' ', '')
        .replaceAll('<tr>', '')
        .replaceAll('<td>', '')
        .replaceAll('</tr>', '\n')
        .replaceAll('</td>', '\t')
    copyToClipboard(asText)
}

const renderLogLines = () => {
    const lines = STORE.logs
        .map(line => {
            const log = JSON.stringify(line, null, 2);
            return {title: line.created + ' ' + line?.run?.id, text: `<pre>${log}</pre>`}
        })
        .map(log => {
            const template = $("#templateLogLine").html();
            return template
                .replaceAll('[[title]]', log.title)
                .replaceAll('[[text]]', log.text)
                .replaceAll('[[id]]', Math.floor(Math.random() * 1000000).toFixed())
        })
        .join('')
    $('#logLines').html(lines)
}
const renderMetricfiles = () => {
    const lines = STORE.metricFiles
        .map(line => {
            return `${line}`
        })
        .join('<br>')
    $('#metricLines').html(lines)
}

const render = () => {
    renderRuns();
    renderRunsMetadata();
    renderTopologies();
}


const addRun = (run, topologyType) => $.post({
    url: '/runs/' + topologyType,
    data: run,
    contentType: 'application/json'
}).done(() => getRuns()).fail(addMessageLog);

const intervalFunctions = [
    () => getTopologies(),
    () => getCluster(),
    () => getPackage(),
    () => getRuns(),
    () => render(),
];

const longPollFunctions = [
    () => nextOnUptime(),
    () => getLogsStormDeploy(),
];

function storage() {
    const initialInterval = getOrSetUptimeInterval();
    if (initialInterval > 0) $('#nextOnUptime' + initialInterval).prop('checked', true);
    const checkboxes = f => $('.uptimeInterval' + (f != null ? f : ''));
    checkboxes().click(function () {
        const checkbox = $(this);
        const value = checkbox.is(':checked') ? checkbox.val() : 0;
        getOrSetUptimeInterval(value)
        checkboxes().prop('checked', false);
        checkbox.prop('checked', value > 0);
    });
}

function getOrSetNextOnUptime(val = undefined) {
    if (val !== undefined) {
        localStorage.setItem(LS_NEXTONUPTIME, val);
    }
    const nextOnUptime = localStorage.getItem(LS_NEXTONUPTIME) === 'true';
    $('#nextOnUptime').prop('checked', nextOnUptime);
    return nextOnUptime;
}

function getOrSetUptimeInterval(val) {
    if (val !== undefined) {
        localStorage.setItem(LS_UPTIME_INTERVAL, val);
    }
    const interval = localStorage.getItem(LS_UPTIME_INTERVAL);
    getOrSetNextOnUptime(interval > 0);
    return interval != null ? parseInt(interval, 10) : 0;
}

function enablePopovers() {
    $('[data-toggle="popover"]').popover('dispose')
    $('[data-toggle="popover"]').popover()
}

$(document).ready(() => {
    enablePopovers();

    storage();
    intervalFunctions.forEach(f => f());
    longPollFunctions.forEach(f => f());

    $('#filterDone').click(() => render());
    $('button#setTopoConfig').click(() => {
        $.post({
            url: '/deploy/config/topo',
            data: $('#topo-config').val(),
            contentType: 'application/json'
        }).done(json => {
            $('#topo-config').val(json)
        }).fail(addMessageLog)
    });
    $('button#addRun').click(() => {
        const run = $('#topo-config').val();
        const topologyType = $('input[name=topologyRadio]:checked').val();
        console.log(topologyType);
        if (topologyType != null) {
            addRun(run, topologyType);
        } else {
            addRun(run, 'WordCountSeal');
            addRun(run, 'WordCountTrident');
        }
    });
    $('button#setStormConfig').click(() => {
        $.post({
            url: '/deploy/config/storm',
            data: $('#storm-config').val(),
            contentType: 'application/yaml'
        }).done(yaml => {
            $('#storm-config').val(yaml)
        }).fail(addMessageLog)
    });
    $('button#deployJar').click(() => {
        $.post({
            url: '/deploy/jar',
            contentType: 'application/yaml'
        }).done().fail(addMessageLog)
    });

    intervalFunctions.forEach(f => setInterval(() => f(), INTERVAL));
    longPollFunctions.forEach(f => setInterval(() => f(), INTERVAL_LONG));
    render();
    // setInterval(() => render(), 1000);
});
